-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.34-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.5.0.5284
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura para tabla bd_personeros.asignacion_centro_votacion
CREATE TABLE IF NOT EXISTS `asignacion_centro_votacion` (
  `dni` char(8) NOT NULL,
  `id_centro_votacion` int(10) unsigned NOT NULL,
  `estado` char(1) NOT NULL,
  PRIMARY KEY (`dni`,`id_centro_votacion`),
  KEY `idx_asignacion_centro_votacion_dni` (`dni`),
  KEY `idx_asignacion_centro_votacion_id_centro_votacion` (`id_centro_votacion`),
  CONSTRAINT `fk_asignacion_centro_votacion_centro_votacion` FOREIGN KEY (`id_centro_votacion`) REFERENCES `centro_votacion` (`id_centro_votacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_asignacion_centro_votacion_personero` FOREIGN KEY (`dni`) REFERENCES `personero` (`dni`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla bd_personeros.asignacion_mesa_sufragio
CREATE TABLE IF NOT EXISTS `asignacion_mesa_sufragio` (
  `dni` char(8) NOT NULL,
  `id_centro_votacion` int(10) unsigned NOT NULL,
  `numero` int(10) unsigned NOT NULL,
  `estado` char(1) NOT NULL,
  PRIMARY KEY (`id_centro_votacion`,`dni`,`numero`),
  KEY `idx_asignacion_mesa_sufragio_dni` (`dni`),
  KEY `idx_asignacion_mesa_sufragio_numero` (`numero`,`id_centro_votacion`),
  CONSTRAINT `fk_asignacion_mesa_sufragio_mesa_sufragio` FOREIGN KEY (`numero`, `id_centro_votacion`) REFERENCES `mesa_sufragio` (`numero`, `id_centro_votacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_asignacion_mesa_sufragio_personero` FOREIGN KEY (`dni`) REFERENCES `personero` (`dni`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla bd_personeros.asistencia
CREATE TABLE IF NOT EXISTS `asistencia` (
  `id_asistencia` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dni` char(8) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_asistencia`),
  KEY `idx_asistencia_dni` (`dni`),
  CONSTRAINT `fk_asistencia_personero` FOREIGN KEY (`dni`) REFERENCES `personero` (`dni`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla bd_personeros.centro_votacion
CREATE TABLE IF NOT EXISTS `centro_votacion` (
  `id_centro_votacion` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) DEFAULT NULL,
  `direccion` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_centro_votacion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla bd_personeros.colaborador
CREATE TABLE IF NOT EXISTS `colaborador` (
  `dni` char(8) NOT NULL,
  `apellido_paterno` varchar(100) DEFAULT NULL,
  `apellido_materno` varchar(100) DEFAULT NULL,
  `nombres` varchar(100) DEFAULT NULL,
  `id_departamento` char(2) NOT NULL,
  `id_provincia` char(2) NOT NULL,
  `id_distrito` char(2) NOT NULL,
  PRIMARY KEY (`dni`),
  KEY `idx_colaborador_id_departamento` (`id_departamento`,`id_provincia`,`id_distrito`),
  CONSTRAINT `fk_colaborador_distrito` FOREIGN KEY (`id_departamento`, `id_provincia`, `id_distrito`) REFERENCES `distrito` (`id_departamento`, `id_provincia`, `id_distrito`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla bd_personeros.departamento
CREATE TABLE IF NOT EXISTS `departamento` (
  `id_departamento` char(2) NOT NULL,
  `nombre` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_departamento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla bd_personeros.distrito
CREATE TABLE IF NOT EXISTS `distrito` (
  `nombre` varchar(150) DEFAULT NULL,
  `id_departamento` char(2) NOT NULL,
  `id_provincia` char(2) NOT NULL,
  `id_distrito` char(2) NOT NULL,
  PRIMARY KEY (`id_departamento`,`id_provincia`,`id_distrito`),
  KEY `idx_distrito_id_departamento` (`id_departamento`,`id_provincia`),
  CONSTRAINT `fk_distrito_provincia` FOREIGN KEY (`id_departamento`, `id_provincia`) REFERENCES `provincia` (`id_departamento`, `id_provincia`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla bd_personeros.mesa_sufragio
CREATE TABLE IF NOT EXISTS `mesa_sufragio` (
  `numero` int(10) unsigned NOT NULL,
  `id_centro_votacion` int(10) unsigned NOT NULL,
  `id_departamento` char(2) NOT NULL,
  `id_provincia` char(2) NOT NULL,
  `id_distrito` char(2) NOT NULL,
  PRIMARY KEY (`numero`,`id_centro_votacion`),
  KEY `idx_mesa_sufragio_id_centro_votacion` (`id_centro_votacion`),
  KEY `idx_mesa_sufragio_id_departamento` (`id_departamento`,`id_provincia`,`id_distrito`),
  CONSTRAINT `fk_mesa_sufragio_centro_votacion` FOREIGN KEY (`id_centro_votacion`) REFERENCES `centro_votacion` (`id_centro_votacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_mesa_sufragio_distrito` FOREIGN KEY (`id_departamento`, `id_provincia`, `id_distrito`) REFERENCES `distrito` (`id_departamento`, `id_provincia`, `id_distrito`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla bd_personeros.padron
CREATE TABLE IF NOT EXISTS `padron` (
  `dni` char(8) NOT NULL,
  `mesa_sufragio` int(10) unsigned NOT NULL,
  `sexo` char(1) NOT NULL,
  `id_tipo_documento` int(10) unsigned NOT NULL,
  `apellido_paterno` varchar(150) DEFAULT NULL,
  `apellido_materno` varchar(150) NOT NULL,
  `nombres` varchar(150) NOT NULL,
  `id_departamento` char(2) NOT NULL,
  `id_provincia` char(2) NOT NULL,
  `id_distrito` char(2) NOT NULL,
  PRIMARY KEY (`dni`),
  KEY `idx_padron_id_tipo_documento` (`id_tipo_documento`),
  KEY `idx_padron_id_departamento` (`id_departamento`,`id_provincia`,`id_distrito`),
  CONSTRAINT `fk_padron_distrito` FOREIGN KEY (`id_departamento`, `id_provincia`, `id_distrito`) REFERENCES `distrito` (`id_departamento`, `id_provincia`, `id_distrito`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_padron_tipo_documento` FOREIGN KEY (`id_tipo_documento`) REFERENCES `tipo_documento` (`id_tipo_documento`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla bd_personeros.personero
CREATE TABLE IF NOT EXISTS `personero` (
  `dni` char(8) NOT NULL,
  `domicilio` varchar(200) DEFAULT NULL,
  `nro_celular_1` varchar(30) DEFAULT NULL,
  `nro_celular_2` varchar(30) DEFAULT NULL,
  `nro_telefonico` varchar(30) DEFAULT NULL,
  `correo` varchar(30) DEFAULT NULL,
  `responsable_dni` char(8) NOT NULL,
  `estado` char(1) DEFAULT NULL,
  `asignacion` char(1) DEFAULT NULL,
  `id_tipo_personero` int(10) unsigned NOT NULL,
  `asignacio_lugar` char(1) DEFAULT NULL,
  PRIMARY KEY (`dni`),
  KEY `idx_personero_responsable_dni` (`responsable_dni`),
  KEY `idx_personero_id_tipo_personero` (`id_tipo_personero`),
  CONSTRAINT `fk_personero_padron` FOREIGN KEY (`dni`) REFERENCES `padron` (`dni`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_personero_personero` FOREIGN KEY (`responsable_dni`) REFERENCES `personero` (`dni`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_personero_tipo_personero` FOREIGN KEY (`id_tipo_personero`) REFERENCES `tipo_personero` (`id_tipo_personero`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla bd_personeros.provincia
CREATE TABLE IF NOT EXISTS `provincia` (
  `nombre` varchar(150) DEFAULT NULL,
  `id_departamento` char(2) NOT NULL,
  `id_provincia` char(2) NOT NULL,
  PRIMARY KEY (`id_departamento`,`id_provincia`),
  KEY `idx_provincia_id_departamento` (`id_departamento`),
  CONSTRAINT `fk_provincia_departamento` FOREIGN KEY (`id_departamento`) REFERENCES `departamento` (`id_departamento`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla bd_personeros.registro_distrital
CREATE TABLE IF NOT EXISTS `registro_distrital` (
  `id_registro_distrital` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `candidato_1` varchar(10) DEFAULT NULL,
  `candidato_2` varchar(10) DEFAULT NULL,
  `candidato_3` varchar(10) NOT NULL,
  `votos_blanco` int(10) unsigned NOT NULL,
  `votos_nulos` int(10) unsigned NOT NULL,
  `votos_impugnados` int(10) unsigned NOT NULL,
  `votos_emitidos` int(10) unsigned NOT NULL,
  `dni` char(8) NOT NULL,
  PRIMARY KEY (`id_registro_distrital`),
  KEY `idx_registro_distrital_dni` (`dni`),
  CONSTRAINT `fk_registro_distrital_usuario` FOREIGN KEY (`dni`) REFERENCES `usuario` (`dni`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla bd_personeros.registro_regional
CREATE TABLE IF NOT EXISTS `registro_regional` (
  `id_registro_regional` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `candidato_1` varchar(10) DEFAULT NULL,
  `candidato_2` varchar(10) DEFAULT NULL,
  `candidato_3` varchar(10) NOT NULL,
  `votos_blanco` int(10) unsigned NOT NULL,
  `votos_nulos` int(10) unsigned NOT NULL,
  `votos_impugnados` int(10) unsigned NOT NULL,
  `votos_emitidos` int(10) unsigned NOT NULL,
  `dni` char(8) NOT NULL,
  PRIMARY KEY (`id_registro_regional`),
  KEY `idx_registro_regional_dni` (`dni`),
  CONSTRAINT `fk_registro_regional_usuario` FOREIGN KEY (`dni`) REFERENCES `usuario` (`dni`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla bd_personeros.tipo_documento
CREATE TABLE IF NOT EXISTS `tipo_documento` (
  `id_tipo_documento` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_tipo_documento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla bd_personeros.tipo_personero
CREATE TABLE IF NOT EXISTS `tipo_personero` (
  `id_tipo_personero` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_tipo_personero`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla bd_personeros.usuario
CREATE TABLE IF NOT EXISTS `usuario` (
  `dni` char(8) NOT NULL,
  `password` char(32) NOT NULL,
  `estado` char(1) NOT NULL,
  PRIMARY KEY (`dni`),
  CONSTRAINT `fk_usuario_colaborador` FOREIGN KEY (`dni`) REFERENCES `colaborador` (`dni`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
