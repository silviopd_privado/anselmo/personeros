$("#m_login_signin_submit").click(function() {

    var usuario = $("#txtusuario").val()
    var password = $("#txtpassword").val()
    var pages = 'P'

    var ruta = DIRECCION_WS + "sesion.validar.php";

    jQuery.post(ruta, { usuario: usuario, password: password, pages: pages }, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {

            Cookies.set('nombre_usuario', datosJSON.datos[0].nombre_usuario);
            Cookies.set('dni', datosJSON.datos[0].dni);
            Cookies.set('id_departamento', datosJSON.datos[0].id_departamento);
            Cookies.set('id_provincia', datosJSON.datos[0].id_provincia);
            Cookies.set('id_distrito', datosJSON.datos[0].id_distrito);
            Cookies.set('ubigeo', datosJSON.datos[0].nombre_departamento + "/" + datosJSON.datos[0].nombre_provincia + "/" + datosJSON.datos[0].nombre_distrito);

            location.href = "./index.html"
        } else {
            swal("Mensaje del sistema", "Datos Incorrectos", "warning");
        }
    }).fail(function(error) {
        swal("Mensaje del sistema", "Datos Incorrectos", "warning");
    })
});