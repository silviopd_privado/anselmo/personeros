(function($) {
    'use strict';
    $(window).on('load', function() {
        $('body').removeClass('m-page--loading');
    });
})(jQuery)

jQuery(document).ready(function($) {
    $("#txtnombre").text(Cookies.get('nombre_usuario'));
    $("#txtubigeo").text(Cookies.get('ubigeo'));
});

//var DIRECCION_WS = "http://localhost:8080/personeros/webservices/webservice/";
var DIRECCION_WS = "http://localhost:8080/anselmo/personeros/webservices/webservice/";


$("#btnSalir").click(function() {
    Cookies.remove('dni');
    Cookies.remove('nombre_usuario');
    Cookies.remove('ubigeo');
    Cookies.remove('id_departamento');
    Cookies.remove('id_provincia');
    Cookies.remove('id_distrito');
    location.href = "./login.html"
});