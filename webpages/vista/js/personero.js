$(document).ready(function() {
    if (Cookies.get('dni') == null) {
        location.href = "./login.html"
    } else {
        //cargarComboDepartamento("#cbodepartamentomodal", "seleccione");
        select_dep();
        cargarComboCandidatos("#cbocandidato", "seleccione");
        $("#cbocandidato").prop('disabled', false);
        $("#btnTerminarHoja").prop('disabled', true);
        $("#btnAgregar").prop('disabled', true);
        $("#btnVerificar").prop('disabled', true);
        $("#dni_respon").prop('disabled', true);
        $("#txt_tipo_respon").val('R');
    }
});

function cargarComboCandidatos(p_nombreCombo, p_tipo) {
    var ruta = DIRECCION_WS + "candidato.listar.php";

    $.post(ruta, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            if (p_tipo === "seleccione") {
                html += '<option value="-">Seleccione un candidato</option>';
            } else {
                html += '<option value="0">Todas los candidatos</option>';
            }

            $.each(datosJSON.datos, function(i, item) {
                html += '<option value="' + item.id_candidato + '">' + item.nombre + '</option>';
            });

            $(p_nombreCombo).html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}



$("#btnVerificar").click(function(event) {
    var dni = $("#dni_respon").val();
    leerDatos_respon(dni);
});
$("#btnVerificar_HOJA").click(function(event) {
    nroLote()
});
$("#btnTerminarHoja").click(function(event) {
    $("#btnVerificar_HOJA").prop('disabled', false);
    $("#btnVerificar").prop('disabled', true);
    $("#btnAgregar").prop('disabled', true);
    $("#name_respon").text("");
    $("#dni_respon").prop('disabled', true);
    $("#dni_respon").val("");
    $("#nro_hoja").text("");
    $("#btnTerminarHoja").prop('disabled', true);
    $("#listado tr").remove();
    $("#listado").empty();
    $("#cbocandidato").prop('disabled', false);
});

function nroLote() {
    swal({
        title: 'INGRESE NRO DE HOJA',
        input: 'text',
        inputAttributes: {
            autocapitalize: 'off'
        },
        showCancelButton: true,
        confirmButtonText: 'Ingresar',
        cancelButtonText: 'Cancelar',
        showLoaderOnConfirm: true,
        preConfirm: (nro) => {
            listar(nro);
        },
        allowOutsideClick: () => !swal.isLoading()
    })
}


$("#modal_select_departamento").change(function() {
    var departamentoid = $("#modal_departamento").val();
    select_prov(departamentoid)
});
$("#modal_select_provincia").change(function() {
    var departamentoid = $("#modal_departamento").val();
    var provinciaid = $("#modal_provincia").val();
    select_distri(departamentoid, provinciaid)
});

function select_dep() {
    var ruta = DIRECCION_WS + "departamento.listar.php";
    $.post(ruta, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<select style="width:400px" class="form-control input-sm" id="modal_departamento">';
            html += '<option value="0" selected>-- seleccione --</option>';
            //Detalle
            $.each(datosJSON.datos, function(i, item) {
                html += '<option value="' + item.id_departamento + '">' + item.nombre + '</option>';
            });

            html += '</select>';

            $("#modal_select_departamento").html(html);

            $("#modal_departamento").select2({
                dropdownParent: $("#modal_select_departamento")
            }).on('change', function(event) {
                event.preventDefault();
            });

        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function select_prov(p_codigo_departamento) {
    var ruta = DIRECCION_WS + "provincia.listar.php";
    $.post(ruta, { codigo_departamento: p_codigo_departamento }, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<select style="width:400px" class="form-control input-sm" id="modal_provincia">';
            html += '<option value="0" selected>-- seleccione --</option>';
            //Detalle
            $.each(datosJSON.datos, function(i, item) {
                html += '<option value="' + item.id_provincia + '">' + item.nombre + '</option>';
            });

            html += '</select>';

            $("#modal_select_provincia").html(html);

            $("#modal_provincia").select2({
                dropdownParent: $("#modal_select_provincia")
            }).on('change', function(event) {
                event.preventDefault();
            });

        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function select_distri(p_codigo_departamento, p_codigo_provincia) {
    var ruta = DIRECCION_WS + "distrito.listar.php";
    $.post(ruta, { codigo_departamento: p_codigo_departamento, codigo_provincia: p_codigo_provincia }, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<select style="width:400px" class="form-control input-sm" id="modal_distrito">';
            html += '<option value="0" selected>-- seleccione --</option>';
            //Detalle
            $.each(datosJSON.datos, function(i, item) {
                html += '<option value="' + item.id_distrito + '">' + item.nombre + '</option>';
            });

            html += '</select>';

            $("#modal_select_distrito").html(html);

            $("#modal_distrito").select2({
                dropdownParent: $("#modal_select_distrito")
            }).on('change', function(event) {
                event.preventDefault();
            });

        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function select_centro() {
    var ruta = DIRECCION_WS + "centro.votacion.listar.ubigeo.php";
    var dep = '13';
    var prov = $("#cboprovinciamodal").val();
    var distri = $("#cbodistritomodal").val();
    $.post(ruta, { dep: dep, prov: prov, distri: distri }, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<select style="width:400px" class="form-control input-sm" id="modal_centro">';
            html += '<option value="0" selected>-- seleccione --</option>';
            //Detalle
            $.each(datosJSON.datos, function(i, item) {
                html += '<option value="' + item.id_centro_votacion + '">' + item.nombre + '</option>';
            });

            html += '</select>';

            $("#modal_select_centro").html(html);

            $("#modal_centro").select2({
                dropdownParent: $("#modal_select_centro")
            }).on('change', function(event) {
                event.preventDefault();
            });

        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function select_mesa() {
    var ruta = DIRECCION_WS + "mesa.listar.x.centro.php";
    var centro = $("#modal_centro").val()
    $.post(ruta, { centro: centro }, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<select style="width:400px" class="form-control input-sm" id="modal_mesa">';
            html += '<option value="0" selected>-- seleccione --</option>';
            //Detalle
            $.each(datosJSON.datos, function(i, item) {
                html += '<option value="' + item.id_centro_votacion + '">' + item.numero + '</option>';
            });

            html += '</select>';

            $("#modal_select_mesa").html(html);

            $("#modal_mesa").select2({
                dropdownParent: $("#modal_select_mesa")
            }).on('change', function(event) {
                event.preventDefault();
            });

        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function select_personero() {
    var ruta = DIRECCION_WS + "personero.autocompletar.listar.php";

    $.post(ruta, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<select style="width:400px" class="form-control input-sm" id="modal_personero">';
            html += '<option value="0" selected>-- seleccione --</option>';
            //Detalle
            $.each(datosJSON.datos, function(i, item) {
                html += '<option value="' + item.dni + '">' + item.nombre + '</option>';
            });

            html += '</select>';

            $("#modal_select_personero").html(html);

            $("#modal_personero").select2({
                dropdownParent: $("#modal_select_personero")
            }).on('change', function(event) {
                event.preventDefault();
            });

        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

$("#btnAgregar").click(function(event) {
    $("#txttipooperacion").val("agregar");
    $("#modal_dni").val("")
    $("#modal_nombre").val("")
    $("#modal_apellido_pat").val("")
    $("#modal_apellido_mat").val("")
    $("#modal_domicilio").val("")
    $("#modal_cel1").val("")
    $("#modal_cel2").val("")
    $("#modal_telefono").val("")
    $("#modal_mesa").val(0).trigger('change');
    $("#cbodepartamentomodal").val("")
    $("#cboprovinciamodal").empty()
    $("#cbodistritomodal").empty()
    $("#modal_correo").val("")
    $("#modal_estado").val("H")
    $("#modal_sexo").val("").trigger('change');
    $("#modal_personero").val(0).trigger('change');
    $("#modal_titulo").text("Agregar Personero");
    var yea = document.getElementById("listado2").rows.length;
    if (yea > 0) {
        $("#txt_tipo_respon").val('P')
    }

});

function listar(nro_hoja) {

    var ruta = DIRECCION_WS + "personero.listar.hoja.php";

    $.post(ruta, { hoja: nro_hoja }, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1" style="width:100%">';
            html += '<thead>';
            html += '<tr>';
            html += '<th>DNI</th>';
            html += '<th>AP PATERNO</th>';
            html += '<th>AP MATERNO</th>';
            html += '<th>NOMBRES</th>';
            html += '<th>DOMICILIO</th>';
            html += '<th>CELULAR 1</th>';
            html += '<th>CELULAR 2</th>';
            html += '<th>TELEFONO</th>';
            html += '<th>CORREO</th>';
            html += '<th>ESTADO</th>';
            html += '<th>OPCIONES</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody id="listado2">';

            //Detalle
            $.each(datosJSON.datos, function(i, item) {
                if (i === 0) {
                    $("#dni_respon").prop('disabled', true);
                    $("#dni_respon").val(item.dni);
                    $("#btnVerificar").prop('disabled', true);
                    $("#btnAgregar").prop('disabled', false);
                    $("#name_respon").text(item.apellido_paterno + ',' + item.nombres);
                    $("#cbocandidato").val(item.id_candidato).change();
                    $("#txt_tipo_respon").val('P')
                } else {
                    html += '<tr>';
                    html += '<td>' + item.dni + '</td>';
                    html += '<td>' + item.apellido_paterno + '</td>';
                    html += '<td>' + item.apellido_materno + '</td>';
                    html += '<td>' + item.nombres + '</td>';
                    html += '<td>' + item.domicilio + '</td>';
                    if (item.nro_celular_1 === null || item.nro_celular_1 === '') {
                        html += '<td>' + '------' + '</td>';
                    } else {
                        html += '<td>' + item.nro_celular_1 + '</td>';
                    }
                    if (item.nro_celular_2 === null || item.nro_celular_2 === '') {
                        html += '<td>' + '------' + '</td>';
                    } else {
                        html += '<td>' + item.nro_celular_2 + '</td>';
                    }
                    if (item.nro_telefonico === null || item.nro_telefonico === '') {
                        html += '<td>' + '------' + '</td>';
                    } else {
                        html += '<td>' + item.nro_telefonico + '</td>';
                    }
                    if (item.correo === null || item.correo === '') {
                        html += '<td>' + '------' + '</td>';
                    } else {
                        html += '<td>' + item.correo + '</td>';
                    }
                    if (item.estado == "H") {
                        html += '<td class="text-center" style="vertical-align:middle"><span style="width: 110px;"><span class="m-badge  m-badge--success m-badge--wide">HABILITADO</span></span></td>';
                    } else {
                        html += '<td class="text-center" style="vertical-align:middle"><span style="width: 110px;"><span class="m-badge  m-badge--danger m-badge--wide">INHABILITADO</span></span></td>';
                    }
                    html += '<td class="text-center">';
                    html += '<button type="button" class="btn btn-sm btn-icon waves-effect waves-light btn-warning" data-toggle="modal" data-target="#signup-modal" onclick="leerDatos(' + "'" + item.dni + "'" + ')"> <i class="flaticon-edit-1"></i> </button>';
                    html += '</td>';
                    html += '</tr>';
                }

            });
            $("#txt_tipo_respon").val('P')

            html += '</tbody>';
            html += '</table>';

            $("#listado").html(html);

            $('#m_table_1').DataTable({
                "responsive": !0,
                "language": {

                    "decimal": "",
                    "emptyTable": "No existe datos",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty": "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered": "(filtrado de _MAX_ total registros)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "No se encontro coincidencias",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                    "aria": {
                        "sortAscending": ": columna ascedente",
                        "sortDescending": ": columna descente"
                    }

                },
                "order": [
                    [0, "asc"]
                ],
                "searching": true,
                "paging": true,
                "info": true
            });
            var yea = document.getElementById("listado2").rows.length;
            if (yea >= 10) {
                $("#btnAgregar").prop('disabled', true);
            }
            $("#nro_hoja").text(nro_hoja);
            $("#btnVerificar_HOJA").prop('disabled', true);
            $("#btnVerificar").prop('disabled', true);
            $("#cbocandidato").prop('disabled', true);
            $("#dni_respon").prop('disabled', true);
            $("#btnTerminarHoja").prop('disabled', false);
        } else {
            swal("Mensaje del sistema", resultado, "warning");


        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal({
            title: 'Ingresar nuevo numero de hoja?',
            text: datosJSON.mensaje,
            type: 'error',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'SI',
            cancelButtonText: 'NO',
        }).then((result) => {
            if (result.value) {
                $("#nro_hoja").text(nro_hoja);
                $("#btnVerificar_HOJA").prop('disabled', true);
                $("#btnVerificar").prop('disabled', false);
                $("#dni_respon").prop('disabled', false);
                $("#btnTerminarHoja").prop('disabled', false);
            }
        })
    })
}

$("#frmgrabar").submit(function(evento) {
    evento.preventDefault();
    var cand = $("#cbocandidato").val();
    if (cand === '-') {
        swal("Mensaje del Sistema", "FALTA SELECIONAR EL CANDIDATO", "error")
        return;
    }

    if ($("#txttipooperacion").val() == "agregar") {

        var ruta = DIRECCION_WS + "personero.agregar.php";
        var dni = $("#modal_dni").val()
        var nombre = $("#modal_nombre").val()
        var ap_pat = $("#modal_apellido_pat").val()
        var ap_mat = $("#modal_apellido_mat").val()
        var domicilio = $("#modal_domicilio").val()
        var cel1 = $("#modal_cel1").val()
        var cel2 = $("#modal_cel2").val()
        var telf = $("#modal_telefono").val()
        var mesa = $("#modal_mesa").val()
        if (mesa === '' || mesa === undefined) {
            mesa = 0;
        }
        var codigo_departamento = $("#modal_departamento").val()
        var codigo_provincia = $("#modal_provincia").val()
        var codigo_distrito = $("#modal_distrito").val()
        var correo = $("#modal_correo").val()
        var estado = $("#modal_estado").val()
        var sexo = $("#modal_sexo").val()

        var person_ref;
        var respon = $("#txt_tipo_respon").val()
        if (respon === 'R') {
            var person_ref = 0;
        } else {
            var person_ref = $("#dni_respon").val()
        }

        var nro_hoja = $("#nro_hoja").text()
        var id_tipo_doc = "1";
        var user = Cookies.get('dni');
        var codigo_candidato = $("#cbocandidato").val()

        swal({
            title: '¿Desea Registrar?',
            text: "se agregará un nuevo Personero!",
            showCancelButton: true,
            confirmButtonClass: 'btn btn-confirm mt-2',
            cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
            confirmButtonText: 'registrar',
            cancelButtonText: 'cancelar',
            imageUrl: "../vista/imagenes/pregunta.png"
        }).then(function() {
            $.post(ruta, {
                dni: dni,
                mesa: mesa,
                sexo: sexo,
                id_tipo_doc: id_tipo_doc,
                ap_pat: ap_pat,
                ap_mat: ap_mat,
                nombres: nombre,
                cod_dep: codigo_departamento,
                cod_prov: codigo_provincia,
                cod_distri: codigo_distrito,
                domicilio: domicilio,
                correo: correo,
                cel_1: cel1,
                cel_2: cel2,
                telef: telf,
                respon_dni: person_ref,
                estado: estado,
                nro_hoja: nro_hoja,
                id_candidato: codigo_candidato,
                dni_user: user
            }, function() {}).done(function(resultado) {
                var datosJSON = resultado;
                if (datosJSON.estado === 200) {
                    swal({
                        title: 'EXITO!',
                        text: datosJSON.mensaje,
                        type: 'success',
                        confirmButtonClass: 'btn btn-confirm mt-2'
                    });
                    $("#signup-modal").modal("hide")
                    listar($("#nro_hoja").text()); //refrescar los datos
                } else {
                    swal("Mensaje del sistema", resultado, "warning");
                }
            }).fail(function(error) {
                var datosJSON = $.parseJSON(error.responseText);
                swal("Error", datosJSON.mensaje, "error");
            })
        });

    } else {
        var ruta = DIRECCION_WS + "personero.editar.php";
        var dni = $("#modal_dni").val()
        var nombre = $("#modal_nombre").val()
        var ap_pat = $("#modal_apellido_pat").val()
        var ap_mat = $("#modal_apellido_mat").val()
        var domicilio = $("#modal_domicilio").val()
        var cel1 = $("#modal_cel1").val()
        var cel2 = $("#modal_cel2").val()
        var telf = $("#modal_telefono").val()
        var mesa = $("#modal_mesa").val()
        if (mesa === '' || mesa === undefined) {
            mesa = 0;
        }
        var codigo_departamento = $("#modal_departamento").val()
        var codigo_provincia = $("#modal_provincia").val()
        var codigo_distrito = $("#modal_distrito").val()
        var correo = $("#modal_correo").val()
        var estado = $("#modal_estado").val()
        var sexo = $("#modal_sexo").val()


        swal({
            title: '¿Desea Modificar?',
            text: "se modificará el personero",
            showCancelButton: true,
            confirmButtonClass: 'btn btn-confirm mt-2',
            cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
            confirmButtonText: 'registrar',
            cancelButtonText: 'cancelar',
            imageUrl: "../vista/imagenes/pregunta2_1.png"
        }).then(function() {
            $.post(ruta, {
                dni: dni,
                mesa: mesa,
                sexo: sexo,
                ap_pat: ap_pat,
                ap_mat: ap_mat,
                nombres: nombre,
                cod_dep: codigo_departamento,
                cod_prov: codigo_provincia,
                cod_distri: codigo_distrito,
                domicilio: domicilio,
                correo: correo,
                cel_1: cel1,
                cel_2: cel2,
                telef: telf,
                estado: estado
            }, function() {}).done(function(resultado) {
                var datosJSON = resultado;
                if (datosJSON.estado === 200) {
                    swal({
                        title: 'EXITO!',
                        text: datosJSON.mensaje,
                        type: 'success',
                        confirmButtonClass: 'btn btn-confirm mt-2'
                    });
                    $("#signup-modal").modal("hide")
                    listar($("#nro_hoja").text()); //refrescar los datos
                } else {
                    swal("Mensaje del sistema", resultado, "warning");
                }
            }).fail(function(error) {
                var datosJSON = $.parseJSON(error.responseText);
                swal("Error", datosJSON.mensaje, "error");
            })
        });
    }

});

function leerDatos_respon(p_dni) {



    var ruta = DIRECCION_WS + "personero.leerdatos.php";

    $.post(ruta, { dni: p_dni }, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            $.each(datosJSON.datos, function(i, item) {


                $("#name_respon").text(item.apellido_paterno + ',' + item.nombres);

                $("#dni_respon").prop('disabled', true);
                $("#btnVerificar").prop('disabled', true);
                swal("Personero encontrado", "ENCONTRADO", "success");
                ingrss_person_respon();
                $("#btnAgregar").prop('disabled', false);
                $("#txt_tipo_respon").val('P')
                $("#cbocandidato").prop('disabled', false);


            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
        swal({
            title: 'Ingresar al Personero?',
            text: datosJSON.mensaje,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'SI',
            cancelButtonText: 'NO',
        }).then((result) => {
            if (result.value) {
                limpíar();
                $("#signup-modal").modal("show")
                $("#txt_tipo_respon").val('R')
                $("#txttipooperacion").val("agregar");
            }
        })
    })
}

function leerDatos(p_dni) {



    var ruta = DIRECCION_WS + "personero.leerdatos.php";

    $.post(ruta, { dni: p_dni }, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            $.each(datosJSON.datos, function(i, item) {
                $("#txttipooperacion").val("editar");

                $("#modal_dni").val(item.dni);
                $("#modal_nombre").val(item.nombres);
                $("#modal_apellido_pat").val(item.apellido_paterno);
                $("#modal_apellido_mat").val(item.apellido_materno);
                $("#modal_select_personero").text(item.responsable_dni);
                $("#modal_domicilio").val(item.domicilio);
                $("#modal_cel1").val(item.nro_celular_1);
                $("#modal_cel2").val(item.nro_celular_2);
                $("#modal_telefono").val(item.nro_telefonico);
                $("#modal_correo").val(item.correo);
                $("#modal_sexo").val(item.sexo);
                $("#modal_estado").val(item.estado);
                $("#modal_mesa").val(item.mesa);
                $("#modal_departamento").val(item.id_departamento).trigger('change');
                $("#modal_provincia").val(item.id_provincia).trigger('change');
                $("#modal_distrito").val(item.id_distrito).trigger('change');

                // $("#cbodistritomodal").val(item.id_distrito)
                // $("#cbodistritomodal").change();
                $("#modal_dni").prop('disabled', true);

                $("#modal_titulo").text("Editar Personero");
            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function limpíar() {
    $("#modal_dni").val("")
    $("#modal_nombre").val("")
    $("#modal_apellido_pat").val("")
    $("#modal_apellido_mat").val("")
    $("#modal_domicilio").val("")
    $("#modal_cel1").val("")
    $("#modal_cel2").val("")
    $("#modal_telefono").val("")
    $("#modal_mesa").val("");
    $("#cbodepartamentomodal").val("")
    $("#cboprovinciamodal").empty()
    $("#cbodistritomodal").empty()
    $("#modal_correo").val("")
    $("#modal_estado").val("H")
    $("#modal_sexo").val("").trigger('change');
}

function ingrss_person_respon() {
    var person_ref = $("#dni_respon").val()
    var nro_hoja = $("#nro_hoja").text()
    var ruta2 = DIRECCION_WS + "nrohoja.agregar.php";
    $.post(ruta2, { dni: person_ref, nro_hoja: nro_hoja }, function() {}).done(function(resultado2) {
        $("#cbocandidato").prop('disabled', true);
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}