jQuery(document).ready(function($) {
    if (Cookies.get('dni') == null) {
        location.href = "./login.html"
    } else {
        listar();
    }
});

function listar() {

    var ruta = DIRECCION_WS + "cedula.provincial.listar.php";
    var dni = Cookies.get('dni');

    $.post(ruta, { dni: dni }, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1" style="width:100%">';
            html += '<thead>';
            html += '<tr>';
            html += '<th>N°</th>';
            html += '<th>NOMBRE</th>';
            html += '<th>CANT</th>';
            html += '<th style="text-align: center">OPCIONES</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody id="detalleconteo">';

            //Detalle
            $.each(datosJSON.datos, function(i, item) {
                html += '<tr>';
                html += '<td>' + item.id_cedula + '</td>';
                html += '<td>' + item.partido + '</td>';
                html += '<td id="txttotal_' + item.id_cedula + '">0</td>';
                html += '<td class="text-center">';
                html += '<button type="button" class="btn btn-sm btn-icon waves-effect waves-light btn-danger" onclick="restar(' + item.id_cedula + ')"> <i class="fa fa-minus"></i> </button>';
                html += '&nbsp';
                html += '<button type="button" class="btn btn-sm btn-icon waves-effect waves-light btn-success" onclick="sumar(' + item.id_cedula + ')"> <i class="fa fa-plus"></i> </button>';
                html += '</td>';
                html += '</tr>';
            });

            html += '</tbody>';
            html += '</table>';

            $("#listado").html(html);

            $('#m_table_1').DataTable({
                "responsive": !0,
                "language": {

                    "decimal": "",
                    "emptyTable": "No existe datos",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty": "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered": "(filtrado de _MAX_ total registros)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "No se encontro coincidencias",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                    "aria": {
                        "sortAscending": ": columna ascedente",
                        "sortDescending": ": columna descente"
                    }

                },
                "order": [
                    [0, "asc"]
                ],
                "searching": false,
                "paging": false,
                "info": false
            });

        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function sumar(id_cedula) {
    var etiqueta = "#txttotal_" + id_cedula
    var total = parseInt($(etiqueta).html())
    var total = total + 1;
    $(etiqueta).html(total)

    votosTotales(1)
}

function restar(id_cedula) {
    var etiqueta = "#txttotal_" + id_cedula
    var total = parseInt($(etiqueta).html())
    if (total > 0) {
        var total = total - 1;
        $(etiqueta).html(total)

        votosTotales(2)
    }
}

$('#txtVotosBlanco').keyup(function() {
    var cantidad = 0
    $("#detalleconteo tr").each(function() {
        cantidad = parseInt($(this).find("td").eq(2).html()) + cantidad;
    });
    var votosBlanco = parseInt($("#txtVotosBlanco").val())
    var votosNulos = parseInt($("#txtVotosNulos").val())
    var votosImpugnados = parseInt($("#txtVotosImpugnados").val())
    if (isNaN(votosBlanco)) {
        votosBlanco = 0
    }
    if (isNaN(votosNulos)) {
        votosNulos = 0
    }
    if (isNaN(votosImpugnados)) {
        votosImpugnados = 0
    }
    $("#txtVotosTotales").val(cantidad + votosBlanco + votosNulos + votosImpugnados)
});

$('#txtVotosNulos').keyup(function() {
    var cantidad = 0
    $("#detalleconteo tr").each(function() {
        cantidad = parseInt($(this).find("td").eq(2).html()) + cantidad;
    });
    var votosBlanco = parseInt($("#txtVotosBlanco").val())
    var votosNulos = parseInt($("#txtVotosNulos").val())
    var votosImpugnados = parseInt($("#txtVotosImpugnados").val())
    if (isNaN(votosBlanco)) {
        votosBlanco = 0
    }
    if (isNaN(votosNulos)) {
        votosNulos = 0
    }
    if (isNaN(votosImpugnados)) {
        votosImpugnados = 0
    }
    $("#txtVotosTotales").val(cantidad + votosBlanco + votosNulos + votosImpugnados)
});

$('#txtVotosImpugnados').keyup(function() {
    var cantidad = 0
    $("#detalleconteo tr").each(function() {
        cantidad = parseInt($(this).find("td").eq(2).html()) + cantidad;
    });
    var votosBlanco = parseInt($("#txtVotosBlanco").val())
    var votosNulos = parseInt($("#txtVotosNulos").val())
    var votosImpugnados = parseInt($("#txtVotosImpugnados").val())
    if (isNaN(votosBlanco)) {
        votosBlanco = 0
    }
    if (isNaN(votosNulos)) {
        votosNulos = 0
    }
    if (isNaN(votosImpugnados)) {
        votosImpugnados = 0
    }
    $("#txtVotosTotales").val(cantidad + votosBlanco + votosNulos + votosImpugnados)
});

function votosTotales(id_tipo) {
    var votos = parseInt($("#txtVotosTotales").val())
    if (id_tipo == 1) {
        votos = votos + 1;
    } else {
        votos = votos - 1;
    }
    $("#txtVotosTotales").val(votos)
}

$("#frmgrabar").submit(function(evento) {
    evento.preventDefault();

    var votosTotales = $("#txtVotosTotales").val()
    var votosBlanco = $("#txtVotosBlanco").val()
    var votosNulos = $("#txtVotosNulos").val()
    var votosImpugnados = $("#txtVotosImpugnados").val()
    var observacion = $("#txtObservacion").val()

    var dni_usuario = Cookies.get('dni')

    var id_departamento = Cookies.get('id_departamento')
    var id_provincia = Cookies.get('id_provincia')
    var id_distrito = Cookies.get('id_distrito')

    var mesa_sufragio = 1;

    var arrayDetalle = new Array();

    swal({
        title: '¿Registrar Conteo?',
        text: "formato para requerimiento de materiales!",
        showCancelButton: true,
        confirmButtonClass: 'btn btn-confirm mt-2',
        cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
        confirmButtonText: 'registrar',
        cancelButtonText: 'cancelar',
        imageUrl: "img/pregunta.png"
    }).then(function() {

        arrayDetalle.splice(0, arrayDetalle.length);

        $("#detalleconteo tr").each(function() {
            var id_cedula = $(this).find("td").eq(0).html();
            var cantidad = $(this).find("td").eq(2).html();

            var objDetalle = new Object();

            objDetalle.id_cedula = id_cedula;
            objDetalle.cantidad = cantidad;

            arrayDetalle.push(objDetalle);
        });

        var jsonDetalle = JSON.stringify(arrayDetalle);

        var ruta = DIRECCION_WS + "registro.conteo.provincial.php";

        $.post(ruta, {
            votosTotales: votosTotales,
            votosBlanco: votosBlanco,
            votosNulos: votosNulos,
            votosImpugnados: votosImpugnados,
            observacion: observacion,
            dni_usuario: dni_usuario,
            id_departamento: id_departamento,
            id_provincia: id_provincia,
            id_distrito: id_distrito,
            mesa_sufragio: mesa_sufragio,
            jsonDetalle: jsonDetalle
        }).done(function(resultado) {
            var datosJSON = resultado;

            if (datosJSON.estado === 200) {
                swal({
                    title: 'EXITO!',
                    text: datosJSON.mensaje,
                    type: 'success',
                    confirmButtonClass: 'btn btn-confirm mt-2'
                });

            } else {
                swal("Mensaje del sistema", resultado, "warning");
            }
        }).fail(function(error) {
            var datosJSON = $.parseJSON(error.responseText);
            swal("Error", datosJSON.mensaje, "error");
        })
    });

})