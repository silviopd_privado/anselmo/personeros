jQuery(document).ready(function() {
    if (Cookies.get('dni') == null) {
        location.href = "./login.html"
    } else {
        listar()
        cargarcomboTipo("#cboTipo", "seleccione")
    }
});

function cargarcomboTipo(p_nombreCombo, p_tipo) {
    var ruta = DIRECCION_WS + "tipo.asistencia.listar.php";

    $.post(ruta, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            if (p_tipo === "seleccione") {
                html += '<option value="-">Seleccione un Tipo</option>';
            } else {
                html += '<option value="0">Todas los Tipos</option>';
            }

            $.each(datosJSON.datos, function(i, item) {
                html += '<option value="' + item.id_tipo + '">' + item.nombre + '</option>';
            });

            $(p_nombreCombo).html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function listar() {

    var ruta = DIRECCION_WS + "personal.asistencia.listar.php";

    $.post(ruta, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1" style="width:100%">';
            html += '<thead>';
            html += '<tr>';
            html += '<th>DNI</th>';
            html += '<th>NOMBRE</th>';
            html += '<th>ASISTENCIA</th>';
            html += '<th>ESCRUTINIO</th>';
            html += '<th>INSTALACION</th>';
            html += '<th>FECHA</th>';
            html += '<th style="text-align: center">OPCIONES</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody id="listado2">';

            //Detalle
            $.each(datosJSON.datos, function(i, item) {
                html += '<tr>';
                html += '<td>' + item.dni + '</td>';
                html += '<td>' + item.nombre_personero + '</td>';
                html += '<td>' + item.asistencia + '</td>';
                html += '<td>' + item.escrutinio + '</td>';
                html += '<td>' + item.instalacion + '</td>';
                html += '<td>' + item.fecha + '</td>';
                html += '<td class="text-center">';
                html += '<button type="button" class="btn btn-sm btn-icon waves-effect waves-light btn-info" data-toggle="modal" data-target="#signup-modal" onclick="validarTipoAsistencia(' + formatNumero(item.dni, 8) + ')"> <i class="mdi mdi-account-check"></i> MARCAR </button>';
                html += '</td>';
                html += '</tr>';
            });

            html += '</tbody>';
            html += '</table>';

            $("#listado").html(html);

            $('#m_table_1').DataTable({
                "responsive": !0,
                "language": {

                    "decimal": "",
                    "emptyTable": "No existe datos",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty": "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered": "(filtrado de _MAX_ total registros)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "No se encontro coincidencias",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                    "aria": {
                        "sortAscending": ": columna ascedente",
                        "sortDescending": ": columna descente"
                    }

                },
                "order": [
                    [0, "asc"]
                ]
            });
            $("#txttipooperacion").val("agregar");
        } else {
            swal("Mensaje del sistema", resultado, "warning");

        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function validarTipoAsistencia(p_dni){
   
    $("#modal_dni").val(p_dni);
    
}
$("#cboTipo").change(function() {
    var id_asistencia = $("#cboTipo").val();
    if (id_asistencia === '-') {
        swal("Mensaje del Sistema", "FALTA SELECIONAR EL TIPO DE ASISTENCIA", "error")
        return;
    }
});
$("#btnRegistrar").click(function(event) {
    var dni = $("#modal_dni").val();
    var id_asistencia = $("#cboTipo").val();
    if (id_asistencia === '-') {
        swal("Mensaje del Sistema", "FALTA SELECIONAR EL TIPO DE ASISTENCIA", "error")
        return;
    }else{
        event.preventDefault();
        marcar(dni, id_asistencia)
    }

});
function marcar(dni,id_asistencia) {
    var ruta = DIRECCION_WS + "registro.personal.asistencia.php";
    var usuario_dni = Cookies.get('dni');
    
    swal({
        title: '¿Desea Registrar?',
        text: "se agregará una asistencia!",
        showCancelButton: true,
        confirmButtonClass: 'btn btn-confirm mt-2',
        cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
        confirmButtonText: 'registrar',
        cancelButtonText: 'cancelar',
        imageUrl: "../vista/imagenes/pregunta.png"
    }).then(function() {
        $.post(ruta, { dni: formatNumero(dni, 8), usuario_dni: formatNumero(usuario_dni, 8),tipo:id_asistencia }, function() {}).done(function(resultado) {
            var datosJSON = resultado;
            if (datosJSON.estado === 200) {
                swal({
                    title: 'EXITO!',
                    text: datosJSON.mensaje,
                    type: 'success',
                    confirmButtonClass: 'btn btn-confirm mt-2'
                });
                $("#signup-modal").modal("hide")
                listar(); //refrescar los datos
            } else {
                swal("Mensaje del sistema", resultado, "warning");
            }
        }).fail(function(error) {
            var datosJSON = $.parseJSON(error.responseText);
            swal("Error", datosJSON.mensaje, "error");
        })
    })
}

function formatNumero(number, width) {
    var numberOutput = Math.abs(number); /* Valor absoluto del número */
    var length = number.toString().length; /* Largo del número */
    var zero = "0"; /* String de cero */

    if (width <= length) {
        if (number < 0) {
            return ("-" + numberOutput.toString());
        } else {
            return numberOutput.toString();
        }
    } else {
        if (number < 0) {
            return ("-" + (zero.repeat(width - length)) + numberOutput.toString());
        } else {
            return ((zero.repeat(width - length)) + numberOutput.toString());
        }
    }
}