<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/CentroVotacion.clase.php';
require_once '../util/funciones/Funciones.clase.php';

$dep = $_POST["dep"];
$prov = $_POST["prov"];
$distri = $_POST["distri"];

try {
    $obj = new CentroVotacion();
    $resultado = $obj->listarXubigeo($dep, $prov, $distri);

    $listavendedores = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "id_centro_votacion" => $resultado[$i]["id_centro_votacion"],
            "nombre" => $resultado[$i]["nombre"]
        );

        $listavendedores[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listavendedores);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
