<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Personero.clase.php';
require_once '../util/funciones/Funciones.clase.php';

$dni = $_POST["dni"];
$mesa = $_POST["mesa"];
$sexo = $_POST["sexo"];
$id_tipo_doc = $_POST["id_tipo_doc"];
$ap_pat = $_POST["ap_pat"];
$ap_mat = $_POST["ap_mat"];
$nomb = $_POST["nombres"];
$cod_dep = $_POST["cod_dep"];
$cod_prov = $_POST["cod_prov"];
$cod_distri = $_POST["cod_distri"];
$domicilio = $_POST["domicilio"];
$cel_1 = $_POST["cel_1"];
$cel_2 = $_POST["cel_2"];
$telef = $_POST["telef"];
$correo = $_POST["correo"];
$respon_dni = $_POST["respon_dni"];
$estado = $_POST["estado"];
$nro_hoja = $_POST["nro_hoja"];
$id_cand = $_POST["id_candidato"];
$dni_user = $_POST["dni_user"];

try {
    if ($respon_dni == 0) {
        $respon_dni = null;
    }

    $obj = new Personero();
    $resultado = $obj->agregar($dni, $mesa, $sexo, $id_tipo_doc, $ap_pat, $ap_mat, $nomb, $cod_dep, $cod_prov, $cod_distri, $domicilio, $cel_1, $cel_2, $telef, $correo, $respon_dni, $estado, $nro_hoja, $id_cand, $dni_user);
    if ($resultado) {
        Funciones::imprimeJSON(200, "Registro Satisfactorio", "");
    } else {
        Funciones::imprimeJSON(500, $exc->getMessage(), "");
    }
} catch (Exception $exc) {
    //Funciones::imprimeJSON(500, $exc->getMessage(), "");
    echo $exc->getMessage();
}
