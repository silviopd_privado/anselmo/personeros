<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Usuario.php';
require_once '../util/funciones/Funciones.clase.php';


try {
    $obj = new Usuario();
    $resultado = $obj->listar_usuarios();

    $listaprovincia = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "dni" => $resultado[$i]["dni"],
            "user" => $resultado[$i]["user"]
        );

        $listaprovincia[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listaprovincia);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}