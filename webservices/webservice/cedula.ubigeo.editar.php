<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/CedulaUbigeo.php';
require_once '../util/funciones/Funciones.clase.php';

try {
    $datoJSONDetalle = $_POST["jsonDetalle"];


//echo $datosFormulario;

    $obj = new CedulaUbigeo();
    $obj->setDetalleUbigeo($datoJSONDetalle);
    $resultado = $obj->editar();

    Funciones::imprimeJSON(200, "Registro ingresado", "");
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}