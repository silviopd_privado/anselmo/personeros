<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Mesa.clase.php';
require_once '../util/funciones/Funciones.clase.php';

$centro = $_POST["centro"];

try {
    $obj = new Mesa();
    $resultado = $obj->listarXcentro($centro);

    $listavendedores = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "numero" => $resultado[$i]["numero"],
            "id_centro_votacion" => $resultado[$i]["id_centro_votacion"]
        );

        $listavendedores[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listavendedores);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
