<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Departamento.clase.php';
require_once '../util/funciones/Funciones.clase.php';

try {

    $obj = new TipoDoc();
    $resultado = $obj->listar();

    $listadepartamento = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "id_tipo_documento" => $resultado[$i]["id_tipo_documento"],
            "nombre" => $resultado[$i]["nombre"]
        );

        $listadepartamento[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listadepartamento);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}