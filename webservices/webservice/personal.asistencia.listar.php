<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Asistencia.php';
require_once '../util/funciones/Funciones.clase.php';

try {

    $obj = new Asistencia();
    $resultado = $obj->listar();

    $listadepartamento = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "dni" => $resultado[$i]["dni"],
            "nombre_personero" => $resultado[$i]["nombre_personero"],
            "asistencia" => $resultado[$i]["asistencia"],
            "escrutinio" => $resultado[$i]["escrutinio"],
            "instalacion" => $resultado[$i]["instalacion"],
            "fecha" => $resultado[$i]["fecha"]
        );

        $listadepartamento[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listadepartamento);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}