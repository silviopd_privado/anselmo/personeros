<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Candidato.clase.php';
require_once '../util/funciones/Funciones.clase.php';

try {

    $obj = new Candidato();
    $resultado = $obj->listar();

    $listadepartamento = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "id_candidato" => $resultado[$i]["id_candidato"],
            "nombre" => $resultado[$i]["nombre"]
        );

        $listadepartamento[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listadepartamento);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}