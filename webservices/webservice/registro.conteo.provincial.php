<?php

header('Access-Control-Allow-Origin: *');
require_once '../negocio/Conteo.php';
require_once '../util/funciones/Funciones.clase.php';

$votosTotales = $_POST["votosTotales"];
$votosBlanco = $_POST["votosBlanco"];
$votosNulos = $_POST["votosNulos"];
$votosImpugnados = $_POST["votosImpugnados"];
$observacion = $_POST["observacion"];

$dni_usuario = $_POST["dni_usuario"];

$id_departamento = $_POST["id_departamento"];
$id_provincia = $_POST["id_provincia"];
$id_distrito = $_POST["id_distrito"];

$mesa_sufragio = $_POST["mesa_sufragio"];

$jsonDetalle = $_POST["jsonDetalle"];

try {
    $objVenta = new Conteo();
    $objVenta->setVotosTotales($votosTotales);
    $objVenta->setVotosBlanco($votosBlanco);
    $objVenta->setVotosNulos($votosNulos);
    $objVenta->setVotosImpugnados($votosImpugnados);
    $objVenta->setObservacion($observacion);

    $objVenta->setDni_usuario($dni_usuario);
    
    $objVenta->setId_departamento($id_departamento);
    $objVenta->setId_provincia($id_provincia);
    $objVenta->setId_distrito($id_distrito);
    
    $objVenta->setMesa_sufragio($mesa_sufragio);

    $objVenta->setJsonDetalle($jsonDetalle);
    $resultado = $objVenta->registrar_conteo_provincial();

    if ($resultado == true) {
        Funciones::imprimeJSON(200, "Registro Satisfactorio", "");
    }
} catch (Exception $exc) {
    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
