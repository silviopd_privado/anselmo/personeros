<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Reporte.php';
require_once '../util/funciones/Funciones.clase.php';


try {
    $obj = new Reporte();
    $resultado = $obj->porcentajesTotales();

    $listaprovincia = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "descripcion" => $resultado[$i]["descripcion"],
            "cantidad" => $resultado[$i]["cantidad"],
            "porcentaje" => $resultado[$i]["porcentaje"]
        );

        $listaprovincia[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listaprovincia);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}