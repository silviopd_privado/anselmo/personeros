<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Usuario.php';
require_once '../util/funciones/Funciones.clase.php';

/*
  if (!isset($_POST["dni"]) || !isset($_POST["clave"])) {
  Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
  exit();
  }
 */

$dni = $_POST["usuario"];
$clave = $_POST["password"];
$pages = $_POST["pages"];

try {
    $objSesion = new Usuario();
    $resultado = $objSesion->iniciarSesionWeb($dni, $clave, $pages);

    if ($resultado[0]["estado"] == 200) {

        /* Generar un token de seguridad */
        //$resultado["token"] = $token;
        /* Generar un token de seguridad */

        Funciones::imprimeJSON(200, "Ingreso al Sistema", $resultado);
    } else {
        Funciones::imprimeJSON(500, $resultado, "");
    }
} catch (Exception $exc) {
    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
