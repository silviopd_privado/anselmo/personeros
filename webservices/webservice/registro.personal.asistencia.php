<?php

header('Access-Control-Allow-Origin: *');
require_once '../negocio/Asistencia.php';
require_once '../util/funciones/Funciones.clase.php';

$dni = $_POST["dni"];
$usuario_dni = $_POST["usuario_dni"];
$tipo = $_POST["tipo"];

try {
    $objVenta = new Asistencia();
    $objVenta->setDni($dni);
    $objVenta->setUsuario_dni($usuario_dni);
    $objVenta->setTipo($tipo);

    $resultado = $objVenta->registrar_asistencia();

    if ($resultado) {
        Funciones::imprimeJSON(200, "Registro Satisfactorio", "");
    }
} catch (Exception $exc) {
    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
