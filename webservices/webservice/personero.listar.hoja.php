<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Personero.clase.php';
require_once '../util/funciones/Funciones.clase.php';

$hoja = $_POST["hoja"];
try {
    $obj = new Personero();
    $resultado = $obj->listar_hoja($hoja);

    $listaprovincia = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "dni" => $resultado[$i]["dni"],
            "apellido_paterno" => $resultado[$i]["apellido_paterno"],
            "apellido_materno" => $resultado[$i]["apellido_materno"],
            "nombres" => $resultado[$i]["nombres"],
            "domicilio" => $resultado[$i]["domicilio"],
            "nro_celular_1" => $resultado[$i]["nro_celular_1"],
            "nro_celular_2" => $resultado[$i]["nro_celular_2"],
            "nro_telefonico" => $resultado[$i]["nro_telefonico"],
            "correo" => $resultado[$i]["correo"],
            "estado" => $resultado[$i]["estado"],
            "id_candidato" => $resultado[$i]["id_candidato"],
        );

        $listaprovincia[$i] = $datos;
    }
    if ((count($listaprovincia) == 0)) {
        throw new Exception("NO SE ENCONTRO PERSONEROS EN LA HOJA " . $hoja . ". INTENTE DENUEVO", 1);
    }
    Funciones::imprimeJSON(200, "", $listaprovincia);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}