<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Reporte.php';
require_once '../util/funciones/Funciones.clase.php';

$dni = $_POST["dni"];
try {
    $obj = new Reporte();
    $resultado = $obj->personeroXusuario($dni);

    $listaprovincia = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "dni" => $resultado[$i]["dni"],
            "apellido_paterno" => $resultado[$i]["apellido_paterno"],
            "apellido_materno" => $resultado[$i]["apellido_materno"],
            "nombres" => $resultado[$i]["nombres"],
            "domicilio" => $resultado[$i]["domicilio"],
            "nro_celular_1" => $resultado[$i]["nro_celular_1"],
            "nro_celular_2" => $resultado[$i]["nro_celular_2"],
            "nro_telefonico" => $resultado[$i]["nro_telefonico"],
            "correo" => $resultado[$i]["correo"],
            "nro_hoja" => $resultado[$i]["nro_hoja"],
            "tipo" => $resultado[$i]["tipo"],
            "estado" => $resultado[$i]["estado"],
            "candidato" => $resultado[$i]["nombre"]
        );

        $listaprovincia[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listaprovincia);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}