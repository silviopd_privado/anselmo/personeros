<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/CedulaUbigeo.php';
require_once '../util/funciones/Funciones.clase.php';

$cod_dep = $_POST["cod_dep"];
$cod_prov = $_POST["cod_prov"];
$cod_distri = $_POST["cod_distri"];

try {
    $obj = new CedulaUbigeo();
    $resultado = $obj->listar_cedula($cod_dep, $cod_prov, $cod_distri);

    $listavendedores = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "id_cedula" => $resultado[$i]["id_cedula"],
            "id_departamento" => $resultado[$i]["id_departamento"],
            "id_provincia" => $resultado[$i]["id_provincia"],
            "id_distrito" => $resultado[$i]["id_distrito"],
            "cedula_provincial" => $resultado[$i]["cedula_provincial"],
            "cedula_distrital" => $resultado[$i]["cedula_distrital"],
            "partido" => $resultado[$i]["partido"]
        );

        $listavendedores[$i] = $datos;
    }
    if ((count($listavendedores) == 0)) {
        throw new Exception("EL DISTRITO NO TIENE AUN CEDULA UBIGEO", 1);
    }
    Funciones::imprimeJSON(200, "", $listavendedores);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
