<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/CedulaUbigeo.php';
require_once '../util/funciones/Funciones.clase.php';

try {

    $obj = new CedulaUbigeo();
    $resultado = $obj->listar();

    $listadepartamento = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "id_cedula" => $resultado[$i]["id_cedula"],
            "partido" => $resultado[$i]["partido"]
        );

        $listadepartamento[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listadepartamento);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}