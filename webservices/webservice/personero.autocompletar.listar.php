<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Personero.clase.php';
require_once '../util/funciones/Funciones.clase.php';


try {

    $obj = new Personero();
    $resultado = $obj->listarAutocompletar();

    $listalab = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "dni" => $resultado[$i]["dni"],
            "nombre" => $resultado[$i]["nombre"]
        );

        $listalab[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listalab);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}