<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/TipoAsistencia.clase.php';
require_once '../util/funciones/Funciones.clase.php';


try {
    $obj = new TipoAsistencia();
    $resultado = $obj->listar();

    $listaprovincia = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "id_tipo" => $resultado[$i]["id_tipo"],
            "nombre" => $resultado[$i]["nombre"]
        );

        $listaprovincia[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listaprovincia);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}