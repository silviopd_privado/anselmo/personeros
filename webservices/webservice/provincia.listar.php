<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Provincia.clase.php';
require_once '../util/funciones/Funciones.clase.php';


if (!isset($_POST["codigo_departamento"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$codigo_departamento = $_POST["codigo_departamento"];

try {
    $obj = new Provincia();
    $resultado = $obj->listar($codigo_departamento);

    $listaprovincia = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "id_departamento" => $resultado[$i]["id_departamento"],
            "id_provincia" => $resultado[$i]["id_provincia"],
            "nombre" => $resultado[$i]["nombre"]
        );

        $listaprovincia[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listaprovincia);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}