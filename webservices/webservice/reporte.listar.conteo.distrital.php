<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Reporte.php';
require_once '../util/funciones/Funciones.clase.php';

try {

    $obj = new Reporte();
    $resultado = $obj->listarConteoDistrital();
    
    $j = 0;

    $listadepartamento = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "conteo" => $resultado[$i]["conteo"],
            "partido" => $resultado[$i]["partido"]
        );

        $listadepartamento[$i] = $datos;
        
        $j = $i;
    }

    $j = $j + 1;
    $datos = array(
        "conteo" => $resultado[0]["votos_blanco"],
        "partido" => "VOTOS EN BLANCO"
    );
    $listadepartamento[$j] = $datos;

    $j = $j + 1;
    $datos = array(
        "conteo" => $resultado[0]["votos_nulos"],
        "partido" => "VOTOS NULOS"
    );
    $listadepartamento[$j] = $datos;

    $j = $j + 1;
    $datos = array(
        "conteo" => $resultado[0]["votos_impugnados"],
        "partido" => "VOTOS IMPURGNADOS"
    );
    $listadepartamento[$j] = $datos;

    Funciones::imprimeJSON(200, "", $listadepartamento);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}