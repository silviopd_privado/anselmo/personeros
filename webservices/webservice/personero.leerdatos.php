<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Personero.clase.php';
require_once '../util/funciones/Funciones.clase.php';

$dni = $_POST["dni"];

try {
    $obj = new Personero();
    $resultado = $obj->leerDatos($dni);

    $listavendedores = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "dni" => $resultado[$i]["dni"],
            "mesa" => $resultado[$i]["mesa_sufragio"],
            "sexo" => $resultado[$i]["sexo"],
            "id_tipo_documento" => $resultado[$i]["id_tipo_documento"],
            "apellido_paterno" => $resultado[$i]["apellido_paterno"],
            "apellido_materno" => $resultado[$i]["apellido_materno"],
            "nombres" => $resultado[$i]["nombres"],
            "id_departamento" => $resultado[$i]["id_departamento"],
            "id_provincia" => $resultado[$i]["id_provincia"],
            "id_distrito" => $resultado[$i]["id_distrito"],
            "domicilio" => $resultado[$i]["domicilio"],
            "nro_celular_1" => $resultado[$i]["nro_celular_1"],
            "nro_celular_2" => $resultado[$i]["nro_celular_2"],
            "nro_telefonico" => $resultado[$i]["nro_telefonico"],
            "correo" => $resultado[$i]["correo"],
            "responsable_dni" => $resultado[$i]["responsable_dni"],
            "estado" => $resultado[$i]["estado"],
            "nro_hoja" => $resultado[$i]["nro_hoja"],
        );

        $listavendedores[$i] = $datos;
    }
    if ((count($listavendedores) == 0)) {
        throw new Exception("NO SE ENCONTRO PERSONERO CON DNI: " . $dni . ". INTENTE DENUEVO", 1);
    }
    Funciones::imprimeJSON(200, "", $listavendedores);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
