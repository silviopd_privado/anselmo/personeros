<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/CedulaProvincial.php';
require_once '../util/funciones/Funciones.clase.php';

$dni = $_POST["dni"];

try {

    $obj = new CedulaProvincial();
    $resultado = $obj->listar($dni);

    $listadepartamento = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "id_cedula" => $resultado[$i]["id_cedula"],
            "partido" => $resultado[$i]["partido"]
        );

        $listadepartamento[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listadepartamento);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}