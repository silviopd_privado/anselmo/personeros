<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Distrito.clase.php';
require_once '../util/funciones/Funciones.clase.php';


if (!isset($_POST["codigo_departamento"]) || !isset($_POST["codigo_provincia"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$codigo_departamento = $_POST["codigo_departamento"];
$codigo_provincia = $_POST["codigo_provincia"];

try {
    $obj = new Distrito();
    $resultado = $obj->listar($codigo_departamento, $codigo_provincia);

    $listadistrito = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "id_departamento" => $resultado[$i]["id_departamento"],
            "id_provincia" => $resultado[$i]["id_provincia"],
            "id_distrito" => $resultado[$i]["id_distrito"],
            "nombre" => $resultado[$i]["nombre"]
        );

        $listadistrito[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listadistrito);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}