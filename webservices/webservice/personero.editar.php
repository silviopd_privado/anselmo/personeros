<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Personero.clase.php';
require_once '../util/funciones/Funciones.clase.php';

$dni = $_POST["dni"];
$mesa = $_POST["mesa"];
$sexo = $_POST["sexo"];
$ap_pat = $_POST["ap_pat"];
$ap_mat = $_POST["ap_mat"];
$nomb = $_POST["nombres"];
$cod_dep = $_POST["cod_dep"];
$cod_prov = $_POST["cod_prov"];
$cod_distri = $_POST["cod_distri"];
$domicilio = $_POST["domicilio"];
$cel_1 = $_POST["cel_1"];
$cel_2 = $_POST["cel_2"];
$telef = $_POST["telef"];
$correo = $_POST["correo"];
$estado = $_POST["estado"];

try {

    $obj = new Personero();
    $resultado = $obj->editar($dni, $mesa, $sexo, $ap_pat, $ap_mat, $nomb, $cod_dep, $cod_prov, $cod_distri, $domicilio, $cel_1, $cel_2, $telef, $correo, $estado);

    if ($resultado) {
        Funciones::imprimeJSON(200, "Modificaciones Guardadas", "");
    } else {
        Funciones::imprimeJSON(500, $exc->getMessage(), "");
    }
} catch (Exception $exc) {
    //Funciones::imprimeJSON(500, $exc->getMessage(), "");
    echo $exc->getMessage();
}
