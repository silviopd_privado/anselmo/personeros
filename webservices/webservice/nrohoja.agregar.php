<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/NroHoja.clase.php';
require_once '../util/funciones/Funciones.clase.php';

$dni = $_POST["dni"];
$nro_hoja = $_POST["nro_hoja"];

try {

    $obj = new NroHoja();
    $resultado = $obj->agregar($dni, $nro_hoja);
    if ($resultado) {
        Funciones::imprimeJSON(200, "Personero agregado a la hoja " . $nro_hoja, "");
    } else {
        Funciones::imprimeJSON(500, $exc->getMessage(), "");
    }
} catch (Exception $exc) {
    //Funciones::imprimeJSON(500, $exc->getMessage(), "");
    echo $exc->getMessage();
}
