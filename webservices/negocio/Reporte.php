<?php

require_once '../datos/Conexion.clase.php';

class Reporte extends Conexion {

    public function listarConteoProvincial() {
        try {
            $sql = "SELECT  ((sum(dcu.conteo)/(cu.votos_emitidos))*100) as conteo,
                            c.partido,
                            ((cu.votos_blanco/(cu.votos_emitidos))*100) as votos_blanco,
                            ((cu.votos_nulos/(cu.votos_emitidos))*100) as votos_nulos,
                            ((cu.votos_impugnados/(cu.votos_emitidos))*100) as votos_impugnados
                    FROM detalle_conteo_ubigeo dcu
                    INNER JOIN cedula c ON ( dcu.id_cedula = c.id_cedula  )
                    INNER JOIN conteo_ubigeo cu ON ( cu.dni = dcu.dni AND cu.id_departamento = dcu.id_departamento AND cu.id_provincia = dcu.id_provincia AND cu.id_distrito = dcu.id_distrito AND cu.provincial_distrital = dcu.provincial_distrital  )
                    WHERE dcu.provincial_distrital = 'P'
                    Group by c.id_cedula";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function listarConteoDistrital() {
        try {
            $sql = "SELECT  ((sum(dcu.conteo)/(cu.votos_emitidos))*100) as conteo,
                            c.partido,
                            ((cu.votos_blanco/(cu.votos_emitidos))*100) as votos_blanco,
                            ((cu.votos_nulos/(cu.votos_emitidos))*100) as votos_nulos,
                            ((cu.votos_impugnados/(cu.votos_emitidos))*100) as votos_impugnados
                    FROM detalle_conteo_ubigeo dcu
                    INNER JOIN cedula c ON ( dcu.id_cedula = c.id_cedula  )
                    INNER JOIN conteo_ubigeo cu ON ( cu.dni = dcu.dni AND cu.id_departamento = dcu.id_departamento AND cu.id_provincia = dcu.id_provincia AND cu.id_distrito = dcu.id_distrito AND cu.provincial_distrital = dcu.provincial_distrital  )
                    WHERE dcu.provincial_distrital = 'D'
                    Group by c.id_cedula";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function personeroXCandidatos() {
        try {
            $sql = "select
	(select count(*) from personero pe where ca.id_candidato=pe.id_candidato and pe.estado='H') as cantidad,
	nombre as 'candidato'
	from candidato ca";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function personeroXusuario($user) {
        try {
            $sql = " CALL `fn_listarpersoneroXusuarios`(:p_dni)";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_dni", $user);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function porcentajesTotales() {
        try {
            $sql = "CALL `fn_rpPorcentajestotale`() ";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
