<?php

require_once '../datos/Conexion.clase.php';

class Asistencia extends Conexion {

    private $dni, $usuario_dni, $tipo;

    function getTipo() {
        return $this->tipo;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    function getDni() {
        return $this->dni;
    }

    function setDni($dni) {
        $this->dni = $dni;
    }

    function getUsuario_dni() {
        return $this->usuario_dni;
    }

    function setUsuario_dni($usuario_dni) {
        $this->usuario_dni = $usuario_dni;
    }

    public function listar() {
        try {
            $sql = "SELECT  p.dni,
concat(p1.apellido_paterno,' ',p1.apellido_materno,' ',p1.nombres) as nombre_personero,
case when (SELECT  count(a.dni) FROM asistencia a where a.dni = p.dni Group by a.dni) is null then 0 else (SELECT  count(a.dni) FROM asistencia a where a.dni = p.dni Group by a.dni) end as asistencia,
case when (SELECT  count(a.dni) FROM asistencia a where a.dni = p.dni and a.id_tipo=2 Group by a.dni) is null then 0 else (SELECT  count(a.dni) FROM asistencia a where a.dni = p.dni and a.id_tipo=2 Group by a.dni) end as escrutinio,
case when (SELECT  count(a.dni) FROM asistencia a where a.dni = p.dni and a.id_tipo=1 Group by a.dni) is null then 0 else (SELECT  count(a.dni) FROM asistencia a where a.dni = p.dni and a.id_tipo=1 Group by a.dni) end as instalacion,
case when (SELECT  date(a.fecha) FROM asistencia a where a.dni = p.dni order by a.fecha desc limit 1) is null then '' else (SELECT  DATE_FORMAT(date(a.fecha), '%d-%m-%y') FROM asistencia a where a.dni = p.dni order by a.fecha desc limit 1) end as fecha
                    FROM personero p INNER JOIN padron p1 ON ( p.dni = p1.dni  )  ";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function registrar_asistencia() {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO asistencia(dni,usuario_dni,id_tipo) values(:p_dni,:p_usuario_dni,:p_id_tipo)";

            $sentencia = $this->dblink->prepare($sql);

            $sentencia->bindValue(":p_dni", $this->getDni());
            $sentencia->bindValue(":p_usuario_dni", $this->getUsuario_dni());
            $sentencia->bindValue(":p_id_tipo", $this->getTipo());
            $sentencia->execute();

            $this->dblink->commit();

            return true;
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw $exc;
        }
        return false;
    }

}
