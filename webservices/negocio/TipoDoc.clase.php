<?php

require_once '../datos/Conexion.clase.php';

class TipoDoc extends Conexion {

    public function listar() {
        try {
            $sql = "select id_tipo_documento,nombre from tipo_documento order by 2";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
