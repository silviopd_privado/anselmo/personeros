<?php

require_once '../datos/Conexion.clase.php';

class Personero extends Conexion {

    private $dni;
    private $domicilio;
    private $nro_cel1;
    private $nro_cel2;
    private $nro_telf;
    private $correo;
    private $responsable_dni;
    private $estado;
    private $asignacion;
    private $asignacion_lugar;
    private $id_tipo_personero;

    function getDni() {
        return $this->dni;
    }

    function getDomicilio() {
        return $this->domicilio;
    }

    function getNro_cel1() {
        return $this->nro_cel1;
    }

    function getNro_cel2() {
        return $this->nro_cel2;
    }

    function getNro_telf() {
        return $this->nro_telf;
    }

    function getCorreo() {
        return $this->correo;
    }

    function getResponsable_dni() {
        return $this->responsable_dni;
    }

    function getEstado() {
        return $this->estado;
    }

    function getAsignacion() {
        return $this->asignacion;
    }

    function getAsignacion_lugar() {
        return $this->asignacion_lugar;
    }

    function getId_tipo_personero() {
        return $this->id_tipo_personero;
    }

    function setDni($dni) {
        $this->dni = $dni;
    }

    function setDomicilio($domicilio) {
        $this->domicilio = $domicilio;
    }

    function setNro_cel1($nro_cel1) {
        $this->nro_cel1 = $nro_cel1;
    }

    function setNro_cel2($nro_cel2) {
        $this->nro_cel2 = $nro_cel2;
    }

    function setNro_telf($nro_telf) {
        $this->nro_telf = $nro_telf;
    }

    function setCorreo($correo) {
        $this->correo = $correo;
    }

    function setResponsable_dni($responsable_dni) {
        $this->responsable_dni = $responsable_dni;
    }

    function setEstado($estado) {
        $this->estado = $estado;
    }

    function setAsignacion($asignacion) {
        $this->asignacion = $asignacion;
    }

    function setAsignacion_lugar($asignacion_lugar) {
        $this->asignacion_lugar = $asignacion_lugar;
    }

    function setId_tipo_personero($id_tipo_personero) {
        $this->id_tipo_personero = $id_tipo_personero;
    }

    public function listar() {
        try {
            $sql = "select pe.dni,
		pa.apellido_paterno,
		pa.apellido_materno,
		pa.nombres,
		pe.domicilio,
		pe.nro_celular_1,
		pe.nro_celular_2,
		pe.nro_telefonico,
		pe.correo,
                pe.estado
	from personero pe inner join
	padron pa on pe.dni=pa.dni
	where pe.estado='H'";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function listar_hoja($hoja) {
        try {
            $sql = "select pe.dni,
		pa.apellido_paterno,
		pa.apellido_materno,
		pa.nombres,
		pe.domicilio,
		pe.nro_celular_1,
		pe.nro_celular_2,
		pe.nro_telefonico,
		pe.correo,
                pe.estado,
                pe.id_candidato
	from personero pe inner join
	padron pa on pe.dni=pa.dni
        	inner join nro_hojas nr on pe.dni=nr.dni
	where pe.estado='H' and nr.nro_hoja=:p_nro_hoja order by pe.responsable_dni asc";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_nro_hoja", $hoja);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function listarAutocompletar() {
        try {
            $sql = "select pe.dni,
		concat(nombres,' ',apellido_paterno,' ',apellido_materno) as nombre
	from personero pe inner join
	padron pa on pe.dni=pa.dni
	where pe.estado='H'";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function agregar($dni, $mesa, $sexo, $id_tipoDoc, $ap_pat, $ap_mat, $nomb, $cod_dep, $cod_prov, $cod_distri, $domicilio, $p_cel1, $p_cel2, $p_telef, $p_correo, $p_dni_respon, $p_estado, $p_nro_hoja, $p_id_candidato, $p_user) {
        $this->dblink->beginTransaction();
        try {
            $sql = "CALL f_agregarPersonero( :p_dni,:p_mesa,:p_sexo,:p_id_tipo_doc,UPPER(:p_ap_pat),"
                    . "UPPER(:p_ap_mat),UPPER(:p_nombres),:p_id_dep,:p_id_prov,:p_id_distri,"
                    . ":p_domicilio,:p_nro_cel1,:p_nro_cel2,:p_nro_telef, :p_correo, "
                    . ":p_respon_dni,:p_estado,:p_nro_hoja,:p_id_candidato,:p_user);";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_dni", $dni);
            $sentencia->bindValue(":p_mesa", $mesa);
            $sentencia->bindValue(":p_sexo", $sexo);
            $sentencia->bindValue(":p_id_tipo_doc", $id_tipoDoc);
            $sentencia->bindValue(":p_ap_pat", $ap_pat);
            $sentencia->bindValue(":p_ap_mat", $ap_mat);
            $sentencia->bindValue(":p_nombres", $nomb);
            $sentencia->bindValue(":p_id_dep", $cod_dep);
            $sentencia->bindValue(":p_id_prov", $cod_prov);
            $sentencia->bindValue(":p_id_distri", $cod_distri);
            $sentencia->bindValue(":p_domicilio", $domicilio);
            $sentencia->bindValue(":p_nro_cel1", $p_cel1);
            $sentencia->bindValue(":p_nro_cel2", $p_cel2);
            $sentencia->bindValue(":p_nro_telef", $p_telef);
            $sentencia->bindValue(":p_correo", $p_correo);
            $sentencia->bindValue(":p_respon_dni", $p_dni_respon);
            $sentencia->bindValue(":p_estado", $p_estado);
            $sentencia->bindValue(":p_nro_hoja", $p_nro_hoja);
            $sentencia->bindValue(":p_id_candidato", $p_id_candidato);
            $sentencia->bindValue(":p_user", $p_user);
            $sentencia->execute();

            $this->dblink->commit();

            return true; //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw $exc;
        }

        return false;
    }

    public function editar($dni, $mesa, $sexo, $ap_pat, $ap_mat, $nomb, $cod_dep, $cod_prov, $cod_distri, $domicilio, $p_cel1, $p_cel2, $p_telef, $p_correo, $p_estado) {
        $this->dblink->beginTransaction();
        try {
            $sql = "update padron pa
		set pa.mesa_sufragio=:p_mesa,
			pa.sexo=:p_sexo,
			pa.apellido_paterno=UPPER(:p_ap_pat),
			pa.apellido_materno=UPPER(:p_ap_mat),
			pa.nombres=UPPER(:p_nombres),
			pa.id_departamento=:p_id_dep,
			pa.id_provincia=:p_id_prov,
			pa.id_distrito=:p_id_distri
			where pa.dni=:p_dni";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_dni", $dni);
            $sentencia->bindValue(":p_mesa", $mesa);
            $sentencia->bindValue(":p_sexo", $sexo);
            $sentencia->bindValue(":p_ap_pat", $ap_pat);
            $sentencia->bindValue(":p_ap_mat", $ap_mat);
            $sentencia->bindValue(":p_nombres", $nomb);
            $sentencia->bindValue(":p_id_dep", $cod_dep);
            $sentencia->bindValue(":p_id_prov", $cod_prov);
            $sentencia->bindValue(":p_id_distri", $cod_distri);
            $sentencia->execute();

            $sql2 = "update personero pe
		set domicilio=:p_domicilio,
			nro_celular_1=:p_nro_cel1,
			nro_celular_2=:p_nro_cel2,
			nro_telefonico=:p_nro_telef,
			correo=:p_correo,
			estado=:p_estado
		where dni=:p_dni";
            $sentencia2 = $this->dblink->prepare($sql2);
            $sentencia2->bindValue(":p_dni", $dni);
            $sentencia2->bindValue(":p_domicilio", $domicilio);
            $sentencia2->bindValue(":p_nro_cel1", $p_cel1);
            $sentencia2->bindValue(":p_nro_cel2", $p_cel2);
            $sentencia2->bindValue(":p_nro_telef", $p_telef);
            $sentencia2->bindValue(":p_correo", $p_correo);
            $sentencia2->bindValue(":p_estado", $p_estado);
            $sentencia2->execute();

            $this->dblink->commit();

            return true; //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw $exc;
        }

        return false;
    }

    public function leerDatos($p_dni) {
        try {
            $sql = "	select
		pa.dni,
		pa.mesa_sufragio,
		pa.sexo,
		pa.id_tipo_documento,
		pa.apellido_paterno,
		pa.apellido_materno,
		pa.nombres,
		pa.id_departamento,
		pa.id_provincia,
		pa.id_distrito,
		pe.domicilio,
		pe.nro_celular_1,
		pe.nro_celular_2,
		pe.nro_telefonico,
		pe.correo,
		pe.responsable_dni,
		pe.estado,
                nr.nro_hoja
		from padron pa
		inner join personero pe on pa.dni=pe.dni
                inner join nro_hojas nr on pe.dni=nr.dni
		where pa.dni=:p_dni limit 1";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_dni", $p_dni);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
