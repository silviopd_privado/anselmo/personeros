<?php

require_once '../datos/Conexion.clase.php';

class Distrito extends Conexion {

    public function listar($p_codigo_departamento, $p_codigo_provincia) {
        try {
            $sql = "SELECT di.id_departamento,di.id_provincia,di.id_distrito,di.nombre from distrito di inner join provincia pr on (di.id_departamento=pr.id_departamento and di.id_provincia=pr.id_provincia) inner join departamento de on pr.id_departamento=de.id_departamento
                       where di.id_departamento =:p_codigo_departamento and di.id_provincia=:p_codigo_provincia";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_codigo_departamento", $p_codigo_departamento);
            $sentencia->bindParam(":p_codigo_provincia", $p_codigo_provincia);
            $sentencia->execute();

            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
