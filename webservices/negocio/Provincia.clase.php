<?php

require_once '../datos/Conexion.clase.php';

class Provincia extends Conexion {

    public function listar($p_codigo_departamento) {
        try {
            $sql = "SELECT pr.id_departamento,pr.id_provincia,pr.nombre from provincia pr inner join departamento de on pr.id_departamento=de.id_departamento where pr.id_departamento= :p_codigo_departamento";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_codigo_departamento", $p_codigo_departamento);
            $sentencia->execute();

            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
