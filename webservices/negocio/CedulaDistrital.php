<?php

require_once '../datos/Conexion.clase.php';

class CedulaDistrital extends Conexion {
    
    public function listar($dni) {
        try {
            $sql = "CALL f_listar_cedula_distrital(:p_dni)";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_dni", $dni);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
}
