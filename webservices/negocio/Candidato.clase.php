<?php

require_once '../datos/Conexion.clase.php';

class Candidato extends Conexion {

    public function listar() {
        try {
            $sql = "select id_candidato,nombre from candidato order by 2";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
