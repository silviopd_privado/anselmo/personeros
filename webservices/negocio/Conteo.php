<?php

require_once '../datos/Conexion.clase.php';

class Conteo extends Conexion {

    private $votosTotales, $votosBlanco, $votosNulos, $votosImpugnados, $observacion, $dni_usuario, $jsonDetalle;
    private $id_departamento, $id_provincia, $id_distrito;
    private $mesa_sufragio;

    function getVotosTotales() {
        return $this->votosTotales;
    }

    function getVotosBlanco() {
        return $this->votosBlanco;
    }

    function getVotosNulos() {
        return $this->votosNulos;
    }

    function getVotosImpugnados() {
        return $this->votosImpugnados;
    }

    function getObservacion() {
        return $this->observacion;
    }

    function getDni_usuario() {
        return $this->dni_usuario;
    }

    function getJsonDetalle() {
        return $this->jsonDetalle;
    }

    function getId_departamento() {
        return $this->id_departamento;
    }

    function getId_provincia() {
        return $this->id_provincia;
    }

    function getId_distrito() {
        return $this->id_distrito;
    }

    function setVotosTotales($votosTotales) {
        $this->votosTotales = $votosTotales;
    }

    function setVotosBlanco($votosBlanco) {
        $this->votosBlanco = $votosBlanco;
    }

    function setVotosNulos($votosNulos) {
        $this->votosNulos = $votosNulos;
    }

    function setVotosImpugnados($votosImpugnados) {
        $this->votosImpugnados = $votosImpugnados;
    }

    function setObservacion($observacion) {
        $this->observacion = $observacion;
    }

    function setDni_usuario($dni_usuario) {
        $this->dni_usuario = $dni_usuario;
    }

    function setJsonDetalle($jsonDetalle) {
        $this->jsonDetalle = $jsonDetalle;
    }

    function setId_departamento($id_departamento) {
        $this->id_departamento = $id_departamento;
    }

    function setId_provincia($id_provincia) {
        $this->id_provincia = $id_provincia;
    }

    function setId_distrito($id_distrito) {
        $this->id_distrito = $id_distrito;
    }
    
    function getMesa_sufragio() {
        return $this->mesa_sufragio;
    }

    function setMesa_sufragio($mesa_sufragio) {
        $this->mesa_sufragio = $mesa_sufragio;
    }

    public function registrar_conteo_provincial() {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO conteo_ubigeo                    
                    (   dni, 
                        id_departamento, 
                        id_provincia, 
                        id_distrito, 
                        mesa_sufragio,
                        provincial_distrital, 
                        votos_blanco, 
                        votos_nulos, 
                        votos_impugnados, 
                        votos_emitidos, 
                        observaciones) 
                    VALUES
                    (   :p_dni, 
                        :p_id_departamento, 
                        :p_id_provincia, 
                        :p_id_distrito, 
                        :p_mesa_sufragio, 
                        :p_provincial_distrital, 
                        :p_votos_blanco, 
                        :p_votos_nulos, 
                        :p_votos_impugnados, 
                        :p_votos_emitidos, 
                        :p_observaciones) ";

            $sentencia = $this->dblink->prepare($sql);

            $sentencia->bindValue(":p_dni", $this->getDni_usuario());
            $sentencia->bindValue(":p_id_departamento", $this->getId_departamento());
            $sentencia->bindValue(":p_id_provincia", $this->getId_provincia());
            $sentencia->bindValue(":p_id_distrito", $this->getId_distrito());
            $sentencia->bindValue(":p_mesa_sufragio", $this->getMesa_sufragio());
            $sentencia->bindValue(":p_provincial_distrital", 'P');
            $sentencia->bindValue(":p_votos_blanco", $this->getVotosBlanco());
            $sentencia->bindValue(":p_votos_nulos", $this->getVotosNulos());
            $sentencia->bindValue(":p_votos_impugnados", $this->getVotosImpugnados());
            $sentencia->bindValue(":p_votos_emitidos", $this->getVotosTotales());
            $sentencia->bindValue(":p_observaciones", $this->getObservacion());

            $sentencia->execute();

            $detalleVentaArray = json_decode($this->getJsonDetalle());

            foreach ($detalleVentaArray as $key => $value) {

                $sql = "INSERT INTO detalle_conteo_ubigeo 
                        (   dni, 
                            id_departamento, 
                            id_provincia, 
                            id_distrito, 
                            mesa_sufragio,
                            provincial_distrital, 
                            id_cedula,
                            conteo)
                        VALUES 
                        (   :p_dni, 
                            :p_id_departamento, 
                            :p_id_provincia, 
                            :p_id_distrito, 
                            :p_mesa_sufragio, 
                            :p_provincial_distrital,
                            :p_id_cedula, 
                            :p_conteo)";

                $sentencia = $this->dblink->prepare($sql);

                $sentencia->bindValue(":p_dni", $this->getDni_usuario());
                $sentencia->bindValue(":p_id_departamento", $this->getId_departamento());
                $sentencia->bindValue(":p_id_provincia", $this->getId_provincia());
                $sentencia->bindValue(":p_id_distrito", $this->getId_distrito());
                $sentencia->bindValue(":p_mesa_sufragio", $this->getMesa_sufragio());
                $sentencia->bindValue(":p_provincial_distrital", 'P');
                $sentencia->bindValue(":p_id_cedula", $value->id_cedula);
                $sentencia->bindValue(":p_conteo", $value->cantidad);

                $sentencia->execute();
            }

            $this->dblink->commit();

            return true;
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw $exc;
        }
        return false;
    }
    
    public function registrar_conteo_distrital() {
        $this->dblink->beginTransaction();
        try {
            $sql = "INSERT INTO conteo_ubigeo                    
                    (   dni, 
                        id_departamento, 
                        id_provincia, 
                        id_distrito, 
                        mesa_sufragio,
                        provincial_distrital, 
                        votos_blanco, 
                        votos_nulos, 
                        votos_impugnados, 
                        votos_emitidos, 
                        observaciones) 
                    VALUES
                    (   :p_dni, 
                        :p_id_departamento, 
                        :p_id_provincia, 
                        :p_id_distrito, 
                        :p_mesa_sufragio, 
                        :p_provincial_distrital, 
                        :p_votos_blanco, 
                        :p_votos_nulos, 
                        :p_votos_impugnados, 
                        :p_votos_emitidos, 
                        :p_observaciones) ";

            $sentencia = $this->dblink->prepare($sql);

            $sentencia->bindValue(":p_dni", $this->getDni_usuario());
            $sentencia->bindValue(":p_id_departamento", $this->getId_departamento());
            $sentencia->bindValue(":p_id_provincia", $this->getId_provincia());
            $sentencia->bindValue(":p_id_distrito", $this->getId_distrito());
            $sentencia->bindValue(":p_mesa_sufragio", $this->getMesa_sufragio());
            $sentencia->bindValue(":p_provincial_distrital", 'D');
            $sentencia->bindValue(":p_votos_blanco", $this->getVotosBlanco());
            $sentencia->bindValue(":p_votos_nulos", $this->getVotosNulos());
            $sentencia->bindValue(":p_votos_impugnados", $this->getVotosImpugnados());
            $sentencia->bindValue(":p_votos_emitidos", $this->getVotosTotales());
            $sentencia->bindValue(":p_observaciones", $this->getObservacion());

            $sentencia->execute();

            $detalleVentaArray = json_decode($this->getJsonDetalle());

            foreach ($detalleVentaArray as $key => $value) {

                $sql = "INSERT INTO detalle_conteo_ubigeo 
                        (   dni, 
                            id_departamento, 
                            id_provincia, 
                            id_distrito, 
                            mesa_sufragio,
                            provincial_distrital, 
                            id_cedula,
                            conteo)
                        VALUES 
                        (   :p_dni, 
                            :p_id_departamento, 
                            :p_id_provincia, 
                            :p_id_distrito, 
                            :p_mesa_sufragio,
                            :p_provincial_distrital,
                            :p_id_cedula, 
                            :p_conteo)";

                $sentencia = $this->dblink->prepare($sql);

                $sentencia->bindValue(":p_dni", $this->getDni_usuario());
                $sentencia->bindValue(":p_id_departamento", $this->getId_departamento());
                $sentencia->bindValue(":p_id_provincia", $this->getId_provincia());
                $sentencia->bindValue(":p_id_distrito", $this->getId_distrito());
                $sentencia->bindValue(":p_mesa_sufragio", $this->getMesa_sufragio());
                $sentencia->bindValue(":p_provincial_distrital", 'D');
                $sentencia->bindValue(":p_id_cedula", $value->id_cedula);
                $sentencia->bindValue(":p_conteo", $value->cantidad);

                $sentencia->execute();
            }

            $this->dblink->commit();

            return true;
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw $exc;
        }
        return false;
    }

}
