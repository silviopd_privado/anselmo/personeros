<?php

require_once '../datos/Conexion.clase.php';

class NroHoja extends Conexion {

    public function listar() {
        try {
            $sql = "select pe.dni,
		pa.apellido_paterno,
		pa.apellido_materno,
		pa.nombres,
		pe.domicilio,
		pe.nro_celular_1,
		pe.nro_celular_2,
		pe.nro_telefonico,
		pe.correo,
                pe.estado
	from personero pe inner join
	padron pa on pe.dni=pa.dni
	where pe.estado='H'";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function listar_repor() {
        try {
            $sql = "	select count(*) as cant,proviene as part from personero where proviene='A' and tipo_respon!='R'
	union
	select count(*) as cant,'G'as part from personero where proviene='G' and tipo_respon!='R'";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function listar_hoja($hoja) {
        try {
            $sql = "select pe.dni,
		pa.apellido_paterno,
		pa.apellido_materno,
		pa.nombres,
		pe.domicilio,
		pe.nro_celular_1,
		pe.nro_celular_2,
		pe.nro_telefonico,
		pe.correo,
                pe.estado,
                pe.id_candidato
	from personero pe inner join
	padron pa on pe.dni=pa.dni
        left join nro_hojas nr on pe.dni=nr.dni
	where pe.estado='H' and pe.nro_hoja=:p_nro_hoja order by pe.responsable_dni asc";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_nro_hoja", $hoja);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function listarAutocompletar() {
        try {
            $sql = "select pe.dni,
		concat(nombres,' ',apellido_paterno,' ',apellido_materno) as nombre
	from personero pe inner join
	padron pa on pe.dni=pa.dni
	where pe.estado='H'";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function agregar($dni, $p_nro_hoja) {
        $this->dblink->beginTransaction();
        try {
            $sql = "insert into nro_hojas(dni,nro_hoja) values(:p_dni,:p_nro_hoja);";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_dni", $dni);
            $sentencia->bindValue(":p_nro_hoja", $p_nro_hoja);
            $sentencia->execute();

            $this->dblink->commit();

            return true; //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw $exc;
        }

        return false;
    }

    public function editar($dni, $mesa, $sexo, $ap_pat, $ap_mat, $nomb, $cod_dep, $cod_prov, $cod_distri, $domicilio, $p_cel1, $p_cel2, $p_telef, $p_correo, $p_dni_respon, $p_estado) {
        $this->dblink->beginTransaction();
        try {
            $sql = "update padron pa
		set pa.mesa_sufragio=:p_mesa,
			pa.sexo=:p_sexo,
			pa.apellido_paterno=UPPER(:p_ap_pat),
			pa.apellido_materno=UPPER(:p_ap_mat),
			pa.nombres=UPPER(:p_nombres),
			pa.id_departamento=:p_id_dep,
			pa.id_provincia=:p_id_prov,
			pa.id_distrito=:p_id_distri
			where pa.dni=:p_dni";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_dni", $dni);
            $sentencia->bindValue(":p_mesa", $mesa);
            $sentencia->bindValue(":p_sexo", $sexo);
            $sentencia->bindValue(":p_ap_pat", $ap_pat);
            $sentencia->bindValue(":p_ap_mat", $ap_mat);
            $sentencia->bindValue(":p_nombres", $nomb);
            $sentencia->bindValue(":p_id_dep", $cod_dep);
            $sentencia->bindValue(":p_id_prov", $cod_prov);
            $sentencia->bindValue(":p_id_distri", $cod_distri);
            $sentencia->execute();

            $sql = "update personero pe
		set domicilio=:p_domicilio,
			nro_celular_1=:p_nro_cel1,
			nro_celular_2=:p_nro_cel2,
			nro_telefonico=:p_nro_telef,
			correo=:p_correo,
			responsable_dni=:p_respon_dni,
			estado=:p_estado
		where dni=:p_dni";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_dni", $dni);
            $sentencia->bindValue(":p_domicilio", $domicilio);
            $sentencia->bindValue(":p_nro_cel1", $p_cel1);
            $sentencia->bindValue(":p_nro_cel2", $p_cel2);
            $sentencia->bindValue(":p_nro_telef", $p_telef);
            $sentencia->bindValue(":p_correo", $p_correo);
            $sentencia->bindValue(":p_respon_dni", $p_dni_respon);
            $sentencia->bindValue(":p_estado", $p_estado);
            $sentencia->execute();

            $this->dblink->commit();

            return true; //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw $exc;
        }

        return false;
    }

    public function leerDatos($p_dni) {
        try {
            $sql = "	select
		pa.dni,
		pa.mesa_sufragio,
		pa.sexo,
		pa.id_tipo_documento,
		pa.apellido_paterno,
		pa.apellido_materno,
		pa.nombres,
		pa.id_departamento,
		pa.id_provincia,
		pa.id_distrito,
		pe.domicilio,
		pe.nro_celular_1,
		pe.nro_celular_2,
		pe.nro_telefonico,
		pe.correo,
		pe.responsable_dni,
		pe.estado,
                pe.nro_hoja
		from padron pa
		inner join personero pe on pa.dni=pe.dni
		where pa.dni=:p_dni";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_dni", $p_dni);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
