<?php

require_once '../datos/Conexion.clase.php';

class Usuario extends Conexion {

    public function iniciarSesionWeb($dni, $clave, $pages) {
        try {
            $sql = "CALL f_inicio_sesion( :p_usuario , md5(:p_password), :p_pages )";
            $sentencia = $this->dblink->prepare($sql);

            $sentencia->bindValue(":p_usuario", $dni);
            $sentencia->bindValue(":p_password", $clave);
            $sentencia->bindValue(":p_pages", $pages);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);

            if ($sentencia->rowCount()) {
                return $resultado;
            } else {
                throw new Exception("Correo o contraseña incorrectos.");
            }
        } catch (PDOException $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function listar_usuarios() {
        try {
            $sql = "select
                u.dni,concat(pa.apellido_paterno,' ',pa.apellido_materno,', ', pa.nombres,' :',u.dni) as user
	from usuario u
	inner join personero pe on u.dni=pe.dni
	inner join padron pa on pa.dni=pe.dni
	where u.dni!='46705014' and pe.estado='H'";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
