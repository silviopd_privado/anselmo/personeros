<?php

require_once '../datos/Conexion.clase.php';

class Mesa extends Conexion {

    private $numero;
    private $id_centro_votacion;
    private $id_departamento;
    private $id_provincia;
    private $id_distrito;

    function getNumero() {
        return $this->numero;
    }

    function getId_centro_votacion() {
        return $this->id_centro_votacion;
    }

    function getId_departamento() {
        return $this->id_departamento;
    }

    function getId_provincia() {
        return $this->id_provincia;
    }

    function getId_distrito() {
        return $this->id_distrito;
    }

    function setNumero($numero) {
        $this->numero = $numero;
    }

    function setId_centro_votacion($id_centro_votacion) {
        $this->id_centro_votacion = $id_centro_votacion;
    }

    function setId_departamento($id_departamento) {
        $this->id_departamento = $id_departamento;
    }

    function setId_provincia($id_provincia) {
        $this->id_provincia = $id_provincia;
    }

    function setId_distrito($id_distrito) {
        $this->id_distrito = $id_distrito;
    }

    public function listarXcentro($id_centro) {
        try {
            $sql = "select numero,id_centro_votacion from mesa_sufragio where id_centro_votacion=:p_id_centro";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_id_centro", $id_centro);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
