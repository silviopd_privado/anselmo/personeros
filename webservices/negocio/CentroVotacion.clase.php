<?php

require_once '../datos/Conexion.clase.php';

class CentroVotacion extends Conexion {

    private $id_centro_votacion;
    private $nombre;
    private $direccion;
    private $id_dep;
    private $id_prov;
    private $id_distri;

    function getId_centro_votacion() {
        return $this->id_centro_votacion;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getDireccion() {
        return $this->direccion;
    }

    function getId_dep() {
        return $this->id_dep;
    }

    function getId_prov() {
        return $this->id_prov;
    }

    function getId_distri() {
        return $this->id_distri;
    }

    function setId_centro_votacion($id_centro_votacion) {
        $this->id_centro_votacion = $id_centro_votacion;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setDireccion($direccion) {
        $this->direccion = $direccion;
    }

    function setId_dep($id_dep) {
        $this->id_dep = $id_dep;
    }

    function setId_prov($id_prov) {
        $this->id_prov = $id_prov;
    }

    function setId_distri($id_distri) {
        $this->id_distri = $id_distri;
    }

    public function listarXubigeo($dep, $prov, $distri) {
        try {
            $sql = "select id_centro_votacion,nombre from centro_votacion where id_departamento=:p_dep and id_provincia=:p_prov and id_distrito=:p_distri";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_dep", $dep);
            $sentencia->bindValue(":p_prov", $prov);
            $sentencia->bindValue(":p_distri", $distri);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
