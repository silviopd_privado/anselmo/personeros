<?php

require_once '../datos/Conexion.clase.php';

class CedulaUbigeo extends Conexion {

    private $detalleUbigeo;
    private $id_cedula;
    private $id_departamento;
    private $id_provincia;
    private $id_distrito;
    private $cedula_provincial;
    private $cedula_distrital;

    function getId_cedula() {
        return $this->id_cedula;
    }

    function getId_departamento() {
        return $this->id_departamento;
    }

    function getId_provincia() {
        return $this->id_provincia;
    }

    function getId_distrito() {
        return $this->id_distrito;
    }

    function getCedula_provincial() {
        return $this->cedula_provincial;
    }

    function getCedula_distrital() {
        return $this->cedula_distrital;
    }

    function setId_cedula($id_cedula) {
        $this->id_cedula = $id_cedula;
    }

    function setId_departamento($id_departamento) {
        $this->id_departamento = $id_departamento;
    }

    function setId_provincia($id_provincia) {
        $this->id_provincia = $id_provincia;
    }

    function setId_distrito($id_distrito) {
        $this->id_distrito = $id_distrito;
    }

    function setCedula_provincial($cedula_provincial) {
        $this->cedula_provincial = $cedula_provincial;
    }

    function setCedula_distrital($cedula_distrital) {
        $this->cedula_distrital = $cedula_distrital;
    }

    function getDetalleUbigeo() {
        return $this->detalleUbigeo;
    }

    function setDetalleUbigeo($detalleUbigeo) {
        $this->detalleUbigeo = $detalleUbigeo;
    }

    public function listar() {
        try {
            $sql = "select * from cedula";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function listar_cedula($cod_dep, $cod_prov, $cod_distri) {
        try {
            $sql = "select cu.id_cedula,
                    cu.id_departamento,
                    cu.id_provincia,
                    cu.id_distrito,
                    cu.cedula_provincial,
                    cu.cedula_distrital,
                    c.partido
                    from cedula_ubigeo cu inner join cedula c on cu.id_cedula=c.id_cedula
	 where id_departamento=:p_id_departamento and id_provincia=:p_id_prov and id_distrito=:p_id_distri and c.estado='H'";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_id_departamento", $cod_dep);
            $sentencia->bindValue(":p_id_prov", $cod_prov);
            $sentencia->bindValue(":p_id_distri", $cod_distri);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function agregar() {
        $this->dblink->beginTransaction();

        try {



            $detalleVentaArray = json_decode($this->getDetalleUbigeo());

            foreach ($detalleVentaArray as $key => $value) {
                $sql = "INSERT INTO cedula_ubigeo
	( id_cedula, id_departamento, id_provincia, id_distrito, cedula_provincial, cedula_distrital)
        VALUES ( :p_id_cedula, :p_id_departamento, :p_id_provincia,:p_id_distrito,:p_cedula_provincial,:p_cedula_distrital);";

                //Preparar la sentencia
                $sentencia = $this->dblink->prepare($sql);

                //Asignar un valor a cada parametro
                $sentencia->bindValue(":p_id_cedula", $value->id_cedula);
                $sentencia->bindValue(":p_id_departamento", $value->id_departamento);
                $sentencia->bindValue(":p_id_provincia", $value->id_provincia);
                $sentencia->bindValue(":p_id_distrito", $value->id_distrito);
                $sentencia->bindValue(":p_cedula_provincial", $value->cedula_prov);
                $sentencia->bindValue(":p_cedula_distrital", $value->cedula_distri);

                $sentencia->execute();
            }

            $this->dblink->commit();

            return true; //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw $exc;
        }

        return false;
    }

    public function editar() {
        $this->dblink->beginTransaction();

        try {
            $detalleVentaArray = json_decode($this->getDetalleUbigeo());

            foreach ($detalleVentaArray as $key => $value) {
                $sql = "update cedula_ubigeo set "
                        . "cedula_provincial= :p_cedula_provincial,"
                        . "cedula_distrital= :p_cedula_distrital "
                        . "where id_cedula=:p_id_cedula and "
                        . "id_departamento=:p_id_departamento and "
                        . "id_provincia= :p_id_provincia "
                        . "and id_distrito=:p_id_distrito;";

                //Preparar la sentencia
                $sentencia = $this->dblink->prepare($sql);

                //Asignar un valor a cada parametro
                $sentencia->bindValue(":p_id_cedula", $value->id_cedula);
                $sentencia->bindValue(":p_id_departamento", $value->id_departamento);
                $sentencia->bindValue(":p_id_provincia", $value->id_provincia);
                $sentencia->bindValue(":p_id_distrito", $value->id_distrito);
                $sentencia->bindValue(":p_cedula_provincial", $value->cedula_prov);
                $sentencia->bindValue(":p_cedula_distrital", $value->cedula_distri);

                $sentencia->execute();
            }

            $this->dblink->commit();

            return true; //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw $exc;
        }

        return false;
    }

}
