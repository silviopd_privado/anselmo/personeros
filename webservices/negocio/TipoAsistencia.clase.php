<?php

require_once '../datos/Conexion.clase.php';

class TipoAsistencia extends Conexion {

    public function listar() {
        try {
            $sql = "select * from tipo_asistencia ";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
