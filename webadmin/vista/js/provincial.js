jQuery(document).ready(function() {
    if (Cookies.get('dni') == null) {
        location.href = "./login.html"
    } else {
        cargarComboDepartamento("#cbodepartamentomodal", "seleccione");
        // $("#cbodepartamentomodal option[value=13]").attr("selected", true);
        // $('#cbodepartamentomodal').change();
        cargarComboProvincia("#cboprovinciamodal", "selecione", '13')
    }
});

$("#cbodepartamentomodal").change(function() {
    var departamentoid = $("#cbodepartamentomodal").val();
    cargarComboProvincia("#cboprovinciamodal", "seleccione", '13');
});

$("#cboprovinciamodal").change(function() {
    var departamentoid = $("#cbodepartamentomodal").val();
    var provinciaid = $("#cboprovinciamodal").val();
    cargarComboDistrito("#cbodistritomodal", "seleccione", '13', provinciaid);
});
$("#cbodistritomodal").change(function() {
    var provinciaid = $("#cboprovinciamodal").val();
    var distritoid = $("#cbodistritomodal").val();
    leerDistrito('13', provinciaid, distritoid);
});



// jQuery(document).ready(function($) {
//     listar();
// });

function listar() {

    var ruta = DIRECCION_WS + "cedula.ubigeo.listar.php";

    $.post(ruta, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1" style="width:100%">';
            html += '<thead>';
            html += '<tr>';
            html += '<th>N°</th>';
            html += '<th>NOMBRE</th>';
            html += '<th style="text-align: center">PROVINCIAL</th>';
            html += '<th style="text-align: center">DISTRITAL</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody id="listado2">';

            //Detalle
            $.each(datosJSON.datos, function(i, item) {
                html += '<tr>';
                html += '<td>' + item.id_cedula + '</td>';
                html += '<td>' + item.partido + '</td>';
                html += '<td class="text-center">';
                html += '<label  class="m-checkbox m-checkbox--success"><input id="chkprov" type="checkbox"><span></span></label>';
                html += '</td>';
                html += '<td class="text-center">';
                html += '<label class="m-checkbox m-checkbox--success"><input id="chkdistri" type="checkbox"><span></span></label>';
                html += '</td>';
                html += '</tr>';
            });

            html += '</tbody>';
            html += '</table>';

            $("#listado").html(html);

            $('#m_table_1').DataTable({
                "responsive": !0,
                "language": {

                    "decimal": "",
                    "emptyTable": "No existe datos",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty": "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered": "(filtrado de _MAX_ total registros)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "No se encontro coincidencias",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                    "aria": {
                        "sortAscending": ": columna ascedente",
                        "sortDescending": ": columna descente"
                    }

                },
                "order": [
                    [0, "asc"]
                ],
                "searching": false,
                "paging": false,
                "info": false
            });
            $("#txttipooperacion").val("agregar");
        } else {
            swal("Mensaje del sistema", resultado, "warning");

        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function leerDistrito(p_dep, p_prov, p_distri) {

    var ruta = DIRECCION_WS + "cedula.ubigeo.leerdatos.php";

    $.post(ruta, { cod_dep: p_dep, cod_prov: p_prov, cod_distri: p_distri }, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1" style="width:100%">';
            html += '<thead>';
            html += '<tr>';
            html += '<th>N°</th>';
            html += '<th>NOMBRE</th>';
            html += '<th style="text-align: center">PROVINCIAL</th>';
            html += '<th style="text-align: center">DISTRITAL</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody id="listado2">';

            //Detalle
            $.each(datosJSON.datos, function(i, item) {
                html += '<tr>';
                html += '<td>' + item.id_cedula + '</td>';
                html += '<td>' + item.partido + '</td>';
                html += '<td class="text-center">';
                if (item.cedula_provincial === 'H') {
                    html += '<label  class="m-checkbox m-checkbox--success"><input id="chkprov" checked="true" type="checkbox"><span></span></label>';
                } else {
                    html += '<label  class="m-checkbox m-checkbox--success"><input id="chkprov" type="checkbox"><span></span></label>';
                }
                html += '</td>';
                html += '<td class="text-center">';
                if (item.cedula_distrital === 'H') {
                    html += '<label class="m-checkbox m-checkbox--success"><input id="chkdistri" checked="true" type="checkbox"><span></span></label>';
                } else {
                    html += '<label class="m-checkbox m-checkbox--success"><input id="chkdistri" type="checkbox"><span></span></label>';
                }

                html += '</td>';
                html += '</tr>';
            });

            html += '</tbody>';
            html += '</table>';

            $("#listado").html(html);

            $('#m_table_1').DataTable({
                "responsive": !0,
                "language": {

                    "decimal": "",
                    "emptyTable": "No existe datos",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty": "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered": "(filtrado de _MAX_ total registros)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "No se encontro coincidencias",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                    "aria": {
                        "sortAscending": ": columna ascedente",
                        "sortDescending": ": columna descente"
                    }

                },
                "searching": false,
                "paging": false,
                "info": false
            });
            $("#txttipooperacion").val("editar");
        } else {
            swal("Mensaje del sistema", resultado, "warning");
            listar();
        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

$("#btnAgregar").submit(function(evento) {
    evento.preventDefault();
    // alert($("#frmgrabar").serialize());
    var arrayDetalle = new Array();
    var id_departamento = 13;
    var id_provincia = $("#cboprovinciamodal").val();
    var id_distrito = $("#cbodistritomodal").val();
    var op = $("#txttipooperacion").val();

    if (op === 'agregar') {

        swal({
            title: '¿Realizar registro?',
            text: "Registrar Cedula de Ubigeo",
            showCancelButton: true,
            confirmButtonClass: 'btn btn-confirm mt-2',
            cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
            confirmButtonText: 'registrar',
            cancelButtonText: 'cancelar',
            imageUrl: "../vista/imagenes/pregunta.png"
        }).then(function() {
            /*limpiar el array*/
            arrayDetalle.splice(0, arrayDetalle.length);
            /*limpiar el array*/

            /*RECORREMOS CADA FILA DE LA TABLA DONDE ESTAN LOS ARTICULOS VENDIDOS*/
            $("#listado2 tr").each(function() {

                var id_cedula = $(this).find("td").eq(0).html();
                var cedula_prov = $(this).find("td").eq(2).find("input").is(":checked");
                var cedula_distri = $(this).find("td").eq(3).find("input").is(":checked");
                if (cedula_prov) {
                    cedula_prov = 'H';
                } else {
                    cedula_prov = 'D'
                }
                if (cedula_distri) {
                    cedula_distri = 'H';
                } else {
                    cedula_distri = 'D'
                }
                //alert($(this).find("td").eq(0).html())
                var objDetalle = new Object(); //se crea un bojeto para guardarlosatributosdela tabla
                objDetalle.id_cedula = id_cedula;
                objDetalle.id_departamento = id_departamento;
                objDetalle.id_provincia = id_provincia;
                objDetalle.id_distrito = id_distrito;
                objDetalle.cedula_prov = cedula_prov;
                objDetalle.cedula_distri = cedula_distri;

                arrayDetalle.push(objDetalle); //agregar el objeto objDetalle al array arrayDetalle
            });

            /*RECORREMOS CADA FILA DE LA TABLA DONDE ESTAN LOS ARTICULOS VENDIDOS*/

            //Convertimos el array "arrayDetalle" a formato de JSON
            var jsonDetalle = JSON.stringify(arrayDetalle);
            /*CAPTURAR TODOS LOS DATOS NECESARIOS PARA GRABAR EN EL VENTA_DETALLE*/
            var ruta = DIRECCION_WS + "cedula.ubigeo.agregar.php";
            // alert(ruta);
            $.post(ruta, {
                jsonDetalle: jsonDetalle
            }).done(function(resultado) {
                var datosJSON = resultado;

                if (datosJSON.estado === 200) {
                    swal({
                        title: 'EXITO!',
                        text: datosJSON.mensaje,
                        type: 'success',
                        confirmButtonClass: 'btn btn-confirm mt-2'
                    });

                    $("#listado").empty();


                } else {
                    swal("Mensaje del sistema", resultado, "warning");
                }
            }).fail(function(error) {
                var datosJSON = $.parseJSON(error.responseText);
                swal("Error", datosJSON.mensaje, "error");
            })
        });
    } else {
        swal({
            title: 'Editar el registro?',
            text: "Registrar Cedula de Ubigeo",
            showCancelButton: true,
            confirmButtonClass: 'btn btn-confirm mt-2',
            cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
            confirmButtonText: 'registrar',
            cancelButtonText: 'cancelar',
            imageUrl: "../vista/imagenes/pregunta.png"
        }).then(function() {
            /*limpiar el array*/
            arrayDetalle.splice(0, arrayDetalle.length);
            /*limpiar el array*/

            /*RECORREMOS CADA FILA DE LA TABLA DONDE ESTAN LOS ARTICULOS VENDIDOS*/
            $("#listado2 tr").each(function() {

                var id_cedula = $(this).find("td").eq(0).html();
                var cedula_prov = $(this).find("td").eq(2).find("input").is(":checked");
                var cedula_distri = $(this).find("td").eq(3).find("input").is(":checked");
                if (cedula_prov) {
                    cedula_prov = 'H';
                } else {
                    cedula_prov = 'D'
                }
                if (cedula_distri) {
                    cedula_distri = 'H';
                } else {
                    cedula_distri = 'D'
                }
                //alert($(this).find("td").eq(0).html())
                var objDetalle = new Object(); //se crea un bojeto para guardarlosatributosdela tabla
                objDetalle.id_cedula = id_cedula;
                objDetalle.id_departamento = id_departamento;
                objDetalle.id_provincia = id_provincia;
                objDetalle.id_distrito = id_distrito;
                objDetalle.cedula_prov = cedula_prov;
                objDetalle.cedula_distri = cedula_distri;

                arrayDetalle.push(objDetalle); //agregar el objeto objDetalle al array arrayDetalle
            });

            /*RECORREMOS CADA FILA DE LA TABLA DONDE ESTAN LOS ARTICULOS VENDIDOS*/

            //Convertimos el array "arrayDetalle" a formato de JSON
            var jsonDetalle = JSON.stringify(arrayDetalle);
            /*CAPTURAR TODOS LOS DATOS NECESARIOS PARA GRABAR EN EL VENTA_DETALLE*/
            var ruta = DIRECCION_WS + "cedula.ubigeo.editar.php";
            // alert(ruta);
            $.post(ruta, {
                jsonDetalle: jsonDetalle
            }).done(function(resultado) {
                var datosJSON = resultado;

                if (datosJSON.estado === 200) {
                    swal({
                        title: 'EXITO!',
                        text: datosJSON.mensaje,
                        type: 'success',
                        confirmButtonClass: 'btn btn-confirm mt-2'
                    });

                    $("#listado").empty();


                } else {
                    swal("Mensaje del sistema", resultado, "warning");
                }
            }).fail(function(error) {
                var datosJSON = $.parseJSON(error.responseText);
                swal("Error", datosJSON.mensaje, "error");
            })
        });

    }

});