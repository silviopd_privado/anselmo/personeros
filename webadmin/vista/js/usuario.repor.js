$(document).ready(function() {
    if (Cookies.get('dni') == null) {
        location.href = "./login.html"
    } else {
        select_usuario()
        listar(0)
    }
});

function select_usuario() {
    var ruta = DIRECCION_WS + "usuario.listar.php";
    $.post(ruta, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<select style="width:400px" class="form-control input-sm" id="modal_usuario">';
            html += '<option value="0" selected>-- TODOS LOS USUARIO --</option>';
            //Detalle
            $.each(datosJSON.datos, function(i, item) {
                html += '<option value="' + item.dni + '">' + item.user + '</option>';
            });

            html += '</select>';

            $("#modal_select_usuario").html(html);

            $("#modal_usuario").select2({
               
            }).on('change', function(event) {
                event.preventDefault();
            });

        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}
$("#modal_select_usuario").change(function() {
    var user = $("#modal_usuario").val();
    $("#listado").empty();
    listar(user)
});
function listar(p_dni) {

    var ruta = DIRECCION_WS + "personero.listar.reporte.xusuario.php";

    $.post(ruta,{dni:p_dni}, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1" style="width:100%">';
            html += '<thead>';
            html += '<tr>';
            html += '<th>DNI</th>';
            html += '<th>AP PATERNO</th>';
            html += '<th>AP MATERNO</th>';
            html += '<th>NOMBRES</th>';
            html += '<th>NRO. HOJA</th>';
            html += '<th>TIPO</th>';
            html += '<th>CANDIDATO</th>';
            html += '<th>DOMICILIO</th>';
            html += '<th>CELULAR 1</th>';
            html += '<th>CELULAR 2</th>';
            html += '<th>TELEFONO</th>';
            html += '<th>CORREO</th>';
            html += '<th>ESTADO</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody id="listado2">';

            //Detalle
            $.each(datosJSON.datos, function(i, item) {
                html += '<tr>';
                html += '<td>' + item.dni + '</td>';
                html += '<td>' + item.apellido_paterno + '</td>';
                html += '<td>' + item.apellido_materno + '</td>';
                html += '<td>' + item.nombres + '</td>';
                html += '<td>' + item.nro_hoja + '</td>';
                html += '<td>' + item.tipo + '</td>';
                html += '<td>' + item.candidato + '</td>';
                html += '<td>' + item.domicilio + '</td>';
                if (item.nro_celular_1 === null || item.nro_celular_1 === '') {
                    html += '<td>' + '------' + '</td>';
                } else {
                    html += '<td>' + item.nro_celular_1 + '</td>';
                }
                if (item.nro_celular_2 === null || item.nro_celular_2 === '') {
                    html += '<td>' + '------' + '</td>';
                } else {
                    html += '<td>' + item.nro_celular_2 + '</td>';
                }
                if (item.nro_telefonico === null || item.nro_telefonico === '') {
                    html += '<td>' + '------' + '</td>';
                } else {
                    html += '<td>' + item.nro_telefonico + '</td>';
                }
                if (item.correo === null || item.correo === '') {
                    html += '<td>' + '------' + '</td>';
                } else {
                    html += '<td>' + item.correo + '</td>';
                }
                if (item.estado == "H") {
                    html += '<td class="text-center" style="vertical-align:middle"><span style="width: 110px;"><span class="m-badge  m-badge--success m-badge--wide">HABILITADO</span></span></td>';
                } else {
                    html += '<td class="text-center" style="vertical-align:middle"><span style="width: 110px;"><span class="m-badge  m-badge--danger m-badge--wide">INHABILITADO</span></span></td>';
                }
                html += '</tr>';


            });

            html += '</tbody>';
            html += '</table>';

            $("#listado").html(html);

            $('#m_table_1').DataTable({
                "responsive": !0,
                "language": {

                    "decimal": "",
                    "emptyTable": "No existe datos",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty": "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered": "(filtrado de _MAX_ total registros)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "No se encontro coincidencias",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                    "aria": {
                        "sortAscending": ": columna ascedente",
                        "sortDescending": ": columna descente"
                    }

                },
                "order": [
                    [0, "asc"]
                ],
                "searching": true,
                "paging": true,
                "info": true
            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");


        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal('Mensaje del sistema', datosJSON.mensaje, 'error');
    })
}