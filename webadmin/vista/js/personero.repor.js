$(document).ready(function() {
    if (Cookies.get('dni') == null) {
        location.href = "./login.html"
    } else {
        listar()
    }
});



function listar() {

    var ruta = DIRECCION_WS + "personero.listar.reporte.php";

    $.post(ruta, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1" style="width:100%">';
            html += '<thead>';
            html += '<tr>';
            html += '<th>CANDIDATO</th>';
            html += '<th>CANTIDAD</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody id="listado2">';

            //Detalle
            $.each(datosJSON.datos, function(i, item) {
                html += '<tr>';
                html += '<td>' + item.candidato + '</td>';
                html += '<td>' + item.cantidad + '</td>';
                html += '</tr>';


            });

            html += '</tbody>';
            html += '</table>';

            $("#listado").html(html);

            $('#m_table_1').DataTable({
                "responsive": !0,
                "language": {

                    "decimal": "",
                    "emptyTable": "No existe datos",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty": "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered": "(filtrado de _MAX_ total registros)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "No se encontro coincidencias",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                    "aria": {
                        "sortAscending": ": columna ascedente",
                        "sortDescending": ": columna descente"
                    }

                },
                "order": [
                    [0, "asc"]
                ],
                "searching": true,
                "paging": true,
                "info": true
            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");


        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal('Mensaje del sistema', datosJSON.mensaje, 'error');
    })
}