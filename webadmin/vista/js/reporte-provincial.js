jQuery(document).ready(function($) {
    if (Cookies.get('dni') == null) {
        location.href = "./login.html"
    } else {
        listar();
        cargarComboDepartamento("#cbodepartamentomodal", "seleccione");
        $("#cbodepartamentomodal").val(13)
        $("#cbodepartamentomodal").change();
    }
});

$("#cbodepartamentomodal").change(function() {
    var departamentoid = $("#cbodepartamentomodal").val();
    cargarComboProvincia("#cboprovinciamodal", "seleccione", departamentoid);
});

$("#cboprovinciamodal").change(function() {
    var departamentoid = $("#cbodepartamentomodal").val();
    var provinciaid = $("#cboprovinciamodal").val();
    cargarComboDistrito("#cbodistritomodal", "seleccione", departamentoid, provinciaid);
});

function listar() {

    var ruta = DIRECCION_WS + "reporte.listar.conteo.provincial.php";

    $.post(ruta, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {

            var chart = AmCharts.makeChart("m_amcharts_1", {
                "type": "serial",
                "theme": "light",
                "dataProvider": datosJSON.datos,
                "valueAxes": [{
                    "gridColor": "#FFFFFF",
                    "gridAlpha": 0.2,
                    "dashLength": 0
                }],
                "gridAboveGraphs": true,
                "startDuration": 1,
                "graphs": [{
                    "balloonText": "[[category]]: <b>[[value]]%</b>",
                    "fillAlphas": 0.8,
                    "lineAlpha": 0.2,
                    "type": "column",
                    "valueField": "conteo"
                }],
                "chartCursor": {
                    "categoryBalloonEnabled": false,
                    "cursorAlpha": 0,
                    "zoomable": false
                },
                "categoryField": "partido",
                "categoryAxis": {
                    "gridPosition": "start",
                    "gridAlpha": 0,
                    "tickPosition": "start",
                    "tickLength": 20,
                    "labelRotation": 90
                },
                "export": {
                    "enabled": true
                }

            });

        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}