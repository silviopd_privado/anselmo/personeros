function cargarComboDepartamento(p_nombreCombo, p_tipo) {
    var ruta = DIRECCION_WS + "departamento.listar.php";

    $.post(ruta, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            if (p_tipo === "seleccione") {
                html += '<option value="">Seleccione una departamento</option>';
            } else {
                html += '<option value="0">Todas los departamentos</option>';
            }

            $.each(datosJSON.datos, function(i, item) {
                html += '<option value="' + item.id_departamento + '">' + item.nombre + '</option>';
            });

            $(p_nombreCombo).html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function cargarComboProvincia(p_nombreCombo, p_tipo, p_codigo_departamento) {
    var ruta = DIRECCION_WS + "provincia.listar.php";

    $.post(ruta, { codigo_departamento: p_codigo_departamento }, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            if (p_tipo === "seleccione") {
                html += '<option value="">Seleccione una provincia</option>';
            } else {
                html += '<option value="0">Todas las provincias</option>';
            }

            $.each(datosJSON.datos, function(i, item) {
                html += '<option value="' + item.id_provincia + '">' + item.nombre + '</option>';
            });

            $(p_nombreCombo).html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function cargarComboDistrito(p_nombreCombo, p_tipo, p_codigo_departamento, p_codigo_provincia) {
    var ruta = DIRECCION_WS + "distrito.listar.php";

    $.post(ruta, { codigo_departamento: p_codigo_departamento, codigo_provincia: p_codigo_provincia }, function() {}).done(function(resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            if (p_tipo === "seleccione") {
                html += '<option value="">Seleccione un distrito</option>';
            } else {
                html += '<option value="0">Todos los distritos</option>';
            }

            $.each(datosJSON.datos, function(i, item) {
                html += '<option value="' + item.id_distrito + '">' + item.nombre + '</option>';
            });

            $(p_nombreCombo).html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function(error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}